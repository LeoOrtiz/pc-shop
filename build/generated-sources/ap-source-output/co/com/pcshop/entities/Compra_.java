package co.com.pcshop.entities;

import co.com.pcshop.entities.Proveedor;
import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Compra.class)
public class Compra_ { 

    public static volatile SingularAttribute<Compra, Double> comCantidad;
    public static volatile SingularAttribute<Compra, Referencia> comReferencia;
    public static volatile SingularAttribute<Compra, Double> comDescuento;
    public static volatile SingularAttribute<Compra, Proveedor> comProveedor;
    public static volatile SingularAttribute<Compra, Date> comFechaCompra;
    public static volatile SingularAttribute<Compra, Integer> comId;
    public static volatile SingularAttribute<Compra, Double> comIva;
    public static volatile SingularAttribute<Compra, Usuario> comUsuario;
    public static volatile SingularAttribute<Compra, Double> comSubtotal;
    public static volatile SingularAttribute<Compra, Double> comPrecio;
    public static volatile SingularAttribute<Compra, Sede> comSede;
    public static volatile SingularAttribute<Compra, Double> comTotal;

}