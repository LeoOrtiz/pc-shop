package co.com.pcshop.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(DescuentorefLispre.class)
public class DescuentorefLispre_ { 

    public static volatile SingularAttribute<DescuentorefLispre, Integer> lispreId;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refsedCantidad;
    public static volatile SingularAttribute<DescuentorefLispre, String> refNota;
    public static volatile SingularAttribute<DescuentorefLispre, String> lispreNombre;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refsedSede;
    public static volatile SingularAttribute<DescuentorefLispre, String> refNombre;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> sedId;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refLinea;
    public static volatile SingularAttribute<DescuentorefLispre, String> sedTelefono;
    public static volatile SingularAttribute<DescuentorefLispre, String> sedNombre;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> reflispreListaPrecio;
    public static volatile SingularAttribute<DescuentorefLispre, String> sedFoto;
    public static volatile SingularAttribute<DescuentorefLispre, Double> reflispreDescuento;
    public static volatile SingularAttribute<DescuentorefLispre, Double> ubiLongitud;
    public static volatile SingularAttribute<DescuentorefLispre, String> marNombre;
    public static volatile SingularAttribute<DescuentorefLispre, Double> reflispreValor;
    public static volatile SingularAttribute<DescuentorefLispre, String> refFoto;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> ubiId;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> sedUbicacion;
    public static volatile SingularAttribute<DescuentorefLispre, Double> ubiLatitud;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> marId;
    public static volatile SingularAttribute<DescuentorefLispre, String> marFoto;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> sedEmpresa;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refsedReferencia;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refId;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refCantidadStock;
    public static volatile SingularAttribute<DescuentorefLispre, String> sedDireccion;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> refMarca;
    public static volatile SingularAttribute<DescuentorefLispre, Integer> reflispreReferencia;

}