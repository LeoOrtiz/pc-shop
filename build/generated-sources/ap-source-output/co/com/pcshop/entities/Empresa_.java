package co.com.pcshop.entities;

import co.com.pcshop.entities.Sede;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:04")
@StaticMetamodel(Empresa.class)
public class Empresa_ { 

    public static volatile SingularAttribute<Empresa, Integer> empId;
    public static volatile SingularAttribute<Empresa, String> empNombre;
    public static volatile SingularAttribute<Empresa, String> empLogo;
    public static volatile CollectionAttribute<Empresa, Sede> sedeCollection;
    public static volatile SingularAttribute<Empresa, String> empDireccion;
    public static volatile SingularAttribute<Empresa, String> empNit;
    public static volatile SingularAttribute<Empresa, String> empRazonSocial;
    public static volatile SingularAttribute<Empresa, String> empTelefono;

}