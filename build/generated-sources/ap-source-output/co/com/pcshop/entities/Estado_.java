package co.com.pcshop.entities;

import co.com.pcshop.entities.Proveedor;
import co.com.pcshop.entities.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:04")
@StaticMetamodel(Estado.class)
public class Estado_ { 

    public static volatile SingularAttribute<Estado, Integer> estId;
    public static volatile CollectionAttribute<Estado, Proveedor> proveedorCollection;
    public static volatile SingularAttribute<Estado, String> estNombre;
    public static volatile CollectionAttribute<Estado, Usuario> usuarioCollection;

}