package co.com.pcshop.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:04")
@StaticMetamodel(GraficaReferencia.class)
public class GraficaReferencia_ { 

    public static volatile SingularAttribute<GraficaReferencia, Integer> refsedCantidad;
    public static volatile SingularAttribute<GraficaReferencia, String> refNota;
    public static volatile SingularAttribute<GraficaReferencia, Integer> refsedSede;
    public static volatile SingularAttribute<GraficaReferencia, String> refNombre;
    public static volatile SingularAttribute<GraficaReferencia, String> refFoto;
    public static volatile SingularAttribute<GraficaReferencia, Integer> sedUbicacion;
    public static volatile SingularAttribute<GraficaReferencia, Integer> sedId;
    public static volatile SingularAttribute<GraficaReferencia, Integer> refLinea;
    public static volatile SingularAttribute<GraficaReferencia, String> sedTelefono;
    public static volatile SingularAttribute<GraficaReferencia, String> sedNombre;
    public static volatile SingularAttribute<GraficaReferencia, Integer> sedEmpresa;
    public static volatile SingularAttribute<GraficaReferencia, Integer> refsedReferencia;
    public static volatile SingularAttribute<GraficaReferencia, Integer> refId;
    public static volatile SingularAttribute<GraficaReferencia, Integer> refCantidadStock;
    public static volatile SingularAttribute<GraficaReferencia, String> sedDireccion;
    public static volatile SingularAttribute<GraficaReferencia, Integer> refMarca;
    public static volatile SingularAttribute<GraficaReferencia, String> sedFoto;

}