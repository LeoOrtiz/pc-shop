package co.com.pcshop.entities;

import co.com.pcshop.entities.Referencia;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Linea.class)
public class Linea_ { 

    public static volatile CollectionAttribute<Linea, Referencia> referenciaCollection;
    public static volatile SingularAttribute<Linea, String> linNombre;
    public static volatile SingularAttribute<Linea, Integer> linId;

}