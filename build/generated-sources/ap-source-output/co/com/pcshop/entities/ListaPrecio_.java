package co.com.pcshop.entities;

import co.com.pcshop.entities.ReferenciaListaPrecio;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(ListaPrecio.class)
public class ListaPrecio_ { 

    public static volatile SingularAttribute<ListaPrecio, Integer> lispreId;
    public static volatile SingularAttribute<ListaPrecio, String> lispreNombre;
    public static volatile CollectionAttribute<ListaPrecio, ReferenciaListaPrecio> referenciaListaPrecioCollection;

}