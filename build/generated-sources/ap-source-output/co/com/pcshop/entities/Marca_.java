package co.com.pcshop.entities;

import co.com.pcshop.entities.Referencia;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Marca.class)
public class Marca_ { 

    public static volatile SingularAttribute<Marca, String> marNombre;
    public static volatile SingularAttribute<Marca, Integer> marId;
    public static volatile SingularAttribute<Marca, String> marFoto;
    public static volatile CollectionAttribute<Marca, Referencia> referenciaCollection;

}