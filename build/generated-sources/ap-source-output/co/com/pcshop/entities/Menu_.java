package co.com.pcshop.entities;

import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.TipoUsuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:04")
@StaticMetamodel(Menu.class)
public class Menu_ { 

    public static volatile SingularAttribute<Menu, String> menuTipo;
    public static volatile SingularAttribute<Menu, Integer> menCodigo;
    public static volatile SingularAttribute<Menu, String> menUrl;
    public static volatile SingularAttribute<Menu, Boolean> menEstado;
    public static volatile SingularAttribute<Menu, String> menNombre;
    public static volatile CollectionAttribute<Menu, Menu> menuCollection;
    public static volatile SingularAttribute<Menu, Menu> menCodigoSubmenu;
    public static volatile SingularAttribute<Menu, TipoUsuario> menuTipoUsuario;

}