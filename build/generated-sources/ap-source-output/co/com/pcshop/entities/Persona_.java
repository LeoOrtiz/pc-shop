package co.com.pcshop.entities;

import co.com.pcshop.entities.TipoDocumento;
import co.com.pcshop.entities.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Persona.class)
public class Persona_ { 

    public static volatile SingularAttribute<Persona, String> perSegundoNombre;
    public static volatile SingularAttribute<Persona, Integer> perId;
    public static volatile SingularAttribute<Persona, String> perPrimerApellido;
    public static volatile SingularAttribute<Persona, String> perSegundoApellido;
    public static volatile SingularAttribute<Persona, String> perPrimerNombre;
    public static volatile SingularAttribute<Persona, TipoDocumento> perTipoDocumento;
    public static volatile SingularAttribute<Persona, Date> perFechaCreacion;
    public static volatile SingularAttribute<Persona, String> perNumeroDocumento;
    public static volatile CollectionAttribute<Persona, Usuario> usuarioCollection;
    public static volatile SingularAttribute<Persona, String> perEmail;

}