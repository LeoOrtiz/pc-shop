package co.com.pcshop.entities;

import co.com.pcshop.entities.Compra;
import co.com.pcshop.entities.Estado;
import co.com.pcshop.entities.TipoDocumento;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Proveedor.class)
public class Proveedor_ { 

    public static volatile SingularAttribute<Proveedor, Estado> proEstado;
    public static volatile SingularAttribute<Proveedor, TipoDocumento> proTipoDocumento;
    public static volatile CollectionAttribute<Proveedor, Compra> compraCollection;
    public static volatile SingularAttribute<Proveedor, String> proNumeroDocumento;
    public static volatile SingularAttribute<Proveedor, Integer> proId;
    public static volatile SingularAttribute<Proveedor, String> proContacto;
    public static volatile SingularAttribute<Proveedor, String> proCompañia;

}