package co.com.pcshop.entities;

import co.com.pcshop.entities.ListaPrecio;
import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.ReferenciaListaPrecioPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:04")
@StaticMetamodel(ReferenciaListaPrecio.class)
public class ReferenciaListaPrecio_ { 

    public static volatile SingularAttribute<ReferenciaListaPrecio, Double> reflispreDescuento;
    public static volatile SingularAttribute<ReferenciaListaPrecio, ReferenciaListaPrecioPK> referenciaListaPrecioPK;
    public static volatile SingularAttribute<ReferenciaListaPrecio, Double> reflispreValor;
    public static volatile SingularAttribute<ReferenciaListaPrecio, ListaPrecio> listaPrecio;
    public static volatile SingularAttribute<ReferenciaListaPrecio, Referencia> referencia;

}