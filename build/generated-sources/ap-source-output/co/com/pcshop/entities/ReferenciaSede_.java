package co.com.pcshop.entities;

import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.ReferenciaSedePK;
import co.com.pcshop.entities.Sede;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(ReferenciaSede.class)
public class ReferenciaSede_ { 

    public static volatile SingularAttribute<ReferenciaSede, Integer> refsedCantidad;
    public static volatile SingularAttribute<ReferenciaSede, Sede> sede;
    public static volatile SingularAttribute<ReferenciaSede, ReferenciaSedePK> referenciaSedePK;
    public static volatile SingularAttribute<ReferenciaSede, Referencia> referencia;

}