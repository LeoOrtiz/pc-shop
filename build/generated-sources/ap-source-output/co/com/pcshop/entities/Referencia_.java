package co.com.pcshop.entities;

import co.com.pcshop.entities.Compra;
import co.com.pcshop.entities.Linea;
import co.com.pcshop.entities.Marca;
import co.com.pcshop.entities.ReferenciaListaPrecio;
import co.com.pcshop.entities.ReferenciaSede;
import co.com.pcshop.entities.Venta;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Referencia.class)
public class Referencia_ { 

    public static volatile SingularAttribute<Referencia, Linea> refLinea;
    public static volatile SingularAttribute<Referencia, String> refNota;
    public static volatile CollectionAttribute<Referencia, Compra> compraCollection;
    public static volatile CollectionAttribute<Referencia, Venta> ventaCollection;
    public static volatile CollectionAttribute<Referencia, ReferenciaSede> referenciaSedeCollection;
    public static volatile SingularAttribute<Referencia, String> refNombre;
    public static volatile SingularAttribute<Referencia, String> refFoto;
    public static volatile SingularAttribute<Referencia, Integer> refId;
    public static volatile SingularAttribute<Referencia, Integer> refCantidadStock;
    public static volatile SingularAttribute<Referencia, Marca> refMarca;
    public static volatile CollectionAttribute<Referencia, ReferenciaListaPrecio> referenciaListaPrecioCollection;

}