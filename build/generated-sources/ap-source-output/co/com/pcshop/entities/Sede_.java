package co.com.pcshop.entities;

import co.com.pcshop.entities.Compra;
import co.com.pcshop.entities.Empresa;
import co.com.pcshop.entities.ReferenciaSede;
import co.com.pcshop.entities.Ubicacion;
import co.com.pcshop.entities.Venta;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Sede.class)
public class Sede_ { 

    public static volatile SingularAttribute<Sede, String> sedTelefono;
    public static volatile CollectionAttribute<Sede, Compra> compraCollection;
    public static volatile CollectionAttribute<Sede, Venta> ventaCollection;
    public static volatile CollectionAttribute<Sede, ReferenciaSede> referenciaSedeCollection;
    public static volatile SingularAttribute<Sede, String> sedNombre;
    public static volatile SingularAttribute<Sede, Empresa> sedEmpresa;
    public static volatile SingularAttribute<Sede, Ubicacion> sedUbicacion;
    public static volatile SingularAttribute<Sede, String> sedDireccion;
    public static volatile SingularAttribute<Sede, Integer> sedId;
    public static volatile SingularAttribute<Sede, String> sedFoto;

}