package co.com.pcshop.entities;

import co.com.pcshop.entities.Persona;
import co.com.pcshop.entities.Proveedor;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(TipoDocumento.class)
public class TipoDocumento_ { 

    public static volatile SingularAttribute<TipoDocumento, Integer> tipdocId;
    public static volatile CollectionAttribute<TipoDocumento, Persona> personaCollection;
    public static volatile SingularAttribute<TipoDocumento, String> tipdocAbreviatura;
    public static volatile CollectionAttribute<TipoDocumento, Proveedor> proveedorCollection;
    public static volatile SingularAttribute<TipoDocumento, String> tipdocNombre;

}