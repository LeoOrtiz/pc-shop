package co.com.pcshop.entities;

import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(TipoUsuario.class)
public class TipoUsuario_ { 

    public static volatile SingularAttribute<TipoUsuario, String> tipusuNombre;
    public static volatile SingularAttribute<TipoUsuario, Integer> tipusuId;
    public static volatile CollectionAttribute<TipoUsuario, Usuario> usuarioCollection;
    public static volatile CollectionAttribute<TipoUsuario, Menu> menuCollection;

}