package co.com.pcshop.entities;

import co.com.pcshop.entities.Sede;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:04")
@StaticMetamodel(Ubicacion.class)
public class Ubicacion_ { 

    public static volatile SingularAttribute<Ubicacion, Double> ubiLongitud;
    public static volatile CollectionAttribute<Ubicacion, Sede> sedeCollection;
    public static volatile SingularAttribute<Ubicacion, Integer> ubiId;
    public static volatile SingularAttribute<Ubicacion, Double> ubiLatitud;

}