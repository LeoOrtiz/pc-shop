package co.com.pcshop.entities;

import co.com.pcshop.entities.Compra;
import co.com.pcshop.entities.Estado;
import co.com.pcshop.entities.ListaPrecio;
import co.com.pcshop.entities.Persona;
import co.com.pcshop.entities.TipoUsuario;
import co.com.pcshop.entities.Venta;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> usuNombre;
    public static volatile SingularAttribute<Usuario, Integer> usuId;
    public static volatile CollectionAttribute<Usuario, Compra> compraCollection;
    public static volatile CollectionAttribute<Usuario, Venta> ventaCollection;
    public static volatile SingularAttribute<Usuario, Persona> usuPersona;
    public static volatile SingularAttribute<Usuario, TipoUsuario> usuTipoUsuario;
    public static volatile SingularAttribute<Usuario, ListaPrecio> usuListaPrecio;
    public static volatile SingularAttribute<Usuario, Date> usuFechaCreacion;
    public static volatile SingularAttribute<Usuario, Estado> usuEstado;
    public static volatile SingularAttribute<Usuario, String> usuClave;

}