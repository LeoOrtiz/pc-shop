package co.com.pcshop.entities;

import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-24T11:58:03")
@StaticMetamodel(Venta.class)
public class Venta_ { 

    public static volatile SingularAttribute<Venta, Date> venFecha;
    public static volatile SingularAttribute<Venta, Integer> venId;
    public static volatile SingularAttribute<Venta, Double> venDescuento;
    public static volatile SingularAttribute<Venta, Usuario> venUsuario;
    public static volatile SingularAttribute<Venta, Double> venSubtotal;
    public static volatile SingularAttribute<Venta, Double> venIva;
    public static volatile SingularAttribute<Venta, Double> venTotal;
    public static volatile SingularAttribute<Venta, Sede> venSede;
    public static volatile SingularAttribute<Venta, Double> venPrecio;
    public static volatile SingularAttribute<Venta, Integer> venCantidad;
    public static volatile SingularAttribute<Venta, Referencia> venReferencia;

}