/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mymapsede;
function initialize_sedes() {
    mymapsede = L.map('mapsede').setView([5.06811, -75.51732], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 22,
        minZoom: 10,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11'
    }).addTo(mymapsede);
    sede();
}

function sede() {
    try {
        $.ajax({
            url: "http://localhost:48020/PC-SHOP/webresources/sede",
            type: 'GET',
            dataType: 'json',
            context: document.body,
            data: {
                // "id": 1
            },
            success: function (datos) {
                mostrar(datos);
            }
        });
    } catch (e) {
        alert(e.message);
    }
}
function mostrar(datos) {
    var sede = datos;
    for (var i = 0; i < sede.length; i++) {
        var latitud = sede[i].sedUbicacion.ubiLatitud;
        var longitud = sede[i].sedUbicacion.ubiLongitud;
        var nombre = sede[i].sedNombre;
        var foto = sede[i].sedFoto;
        var telefono = sede[i].sedTelefono;
        var direccion = sede[i].sedDireccion;



        var id = sede[i].sedId;
        var icono;

        icono = L.icon({
            iconUrl: 'http://localhost:48020/PC-SHOP/resources/images/icon-sede.png',
            iconSize: [33, 33]
        });

        addMarker(latitud, longitud, nombre, foto, telefono, direccion, id, icono);
    }
}
function addMarker(latitud, longitud, nombre, foto, telefono, direccion, content, icon) {
    L.marker([latitud, longitud], {icon: icon}).addTo(mymapsede)
            .bindPopup("<b><h4>" + nombre + "</h4></b>" +
                    "<img style='height: 120px;width: 150px; margin: 0 auto; display: block'; src='http://localhost:48020/PC-SHOP/resources/images/sedes/" + foto + "'/>" +
                    "<br/> <b>Telefono: </b>" + telefono +
                    "<br/> <b>Direccion: </b>" + direccion);
}
