package co.com.pcshop.controller;

import co.com.pcshop.entities.Compra;
import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.Proveedor;
import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Usuario;
import co.com.pcshop.facade.local.CompraFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mauricio-veronica
 */
@ManagedBean
@SessionScoped
public class CompraController implements Serializable {

    @EJB
    private CompraFacadeLocal compraEJB;
    
    //SE CREAN LOS OBJECTOS
    private Compra compra;
    private Proveedor proveedor;
    private Usuario usuario;
    private Referencia referencia;
    private Sede sede;
    
    
    //RECUPERA LOS VALORES DE LOS h:selectOneMenu DE LA VISTA
    private int idproveedor;
    private int idreferencia;
    private int idsede;
    
    //RECUPERA LOS VALORES DE LOS InputText
    private double cantidad;
    private double descuento;
    private double precio;
    
    List<Compra> listacompra = null;
    
    String mensaje = " ";
    
    
    
    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public int getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(int idproveedor) {
        this.idproveedor = idproveedor;
    }
    
    public List<Compra> getListacompra() {

        if (listacompra == null) {
            listacompra = compraEJB.findAll();
        }
        listacompra = compraEJB.findAll();
        return listacompra;
    }

    public void setListacompra(List<Compra> listacompra) {
        this.listacompra = listacompra;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public int getIdreferencia() {
        return idreferencia;
    }

    public void setIdreferencia(int idreferencia) {
        this.idreferencia = idreferencia;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }
    
    
    
    
    @PostConstruct
    public void init() {
        compra = new Compra();//genera una instancia al objeto

    }

    public void guardar() {
        try {
            double iva =((cantidad*precio)*0.19);
            compra.setComIva(iva);
            compra.setComCantidad(cantidad);
            compra.setComPrecio(precio);
            compra.setComSubtotal((cantidad*precio)+iva);
            compra.setComDescuento(descuento);
            compra.setComTotal((cantidad*precio)+(iva-descuento));
            compra.setComReferencia(new Referencia(idreferencia));
            compra.setComProveedor(new Proveedor(idproveedor));
            compra.setComSede(new Sede(idsede));
            
            FacesContext context = FacesContext.getCurrentInstance();
            Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
            int user =us.getUsuId();
            compra.setComUsuario(new Usuario(user));
            
            compraEJB.create(compra);
            compra = new Compra();
            this.mensaje = "Registrado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            compra = new Compra();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        compra = new Compra();
        listacompra = compraEJB.findAll();
    }

//  
    public void cargarId(Compra p) {
        this.compra = p;
    }

    public void editar() {
        try {
            double iva =((cantidad*precio)*0.19);
            compra.setComIva(iva);
            compra.setComCantidad(cantidad);
            compra.setComPrecio(precio);
            compra.setComSubtotal((cantidad*precio)+iva);
            compra.setComDescuento(descuento);
            compra.setComTotal((cantidad*precio)+(iva-descuento));
            compra.setComReferencia(new Referencia(idreferencia));
            compra.setComProveedor(new Proveedor(idproveedor));
            compra.setComSede(new Sede(idsede));
            
            FacesContext context = FacesContext.getCurrentInstance();
            Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
            int user =us.getUsuId();
            compra.setComUsuario(new Usuario(user));
            
            compraEJB.edit(compra);
            compra = new Compra();
            this.mensaje = " editado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listacompra = compraEJB.findAll();
    }

    public void limpiar() {
        this.compra = new Compra();
    }

    public void eliminar(Compra p) {
        try {
            compraEJB.remove(p);
            this.mensaje = "eliminado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listacompra = compraEJB.findAll();
    }
}