package co.com.pcshop.controller;

import co.com.pcshop.entities.Empresa;
import co.com.pcshop.facade.local.EmpresaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class EmpresaController implements Serializable {
    @EJB
    private EmpresaFacadeLocal EmpresaFacade;
    private List<Empresa> listaEmpresa;
    private Empresa Empresa;
    String mensaje="";

    public List<Empresa> getListaEmpresa() {
        this.listaEmpresa= this.EmpresaFacade.findAll();
        return listaEmpresa;
    }

    public void setListaEmpresa(List<Empresa> listaEmpresa) {
        this.listaEmpresa = listaEmpresa;
    }

    public Empresa getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(Empresa Empresa) {
        this.Empresa = Empresa;
    }
    
    @PostConstruct   
    public void init(){
        this.Empresa = new Empresa();
    }
    
    public void cargaId (Empresa e){
        this.Empresa = e;
    }
    
    public void editar(){
        try{
            this.EmpresaFacade.edit(Empresa);
            this.mensaje="Se modifico el Empresa Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        this.Empresa = new Empresa();
    }
}