package co.com.pcshop.controller;

import co.com.pcshop.entities.Estado;
import co.com.pcshop.facade.local.EstadoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class EstadoController implements Serializable {
    @EJB
    private EstadoFacadeLocal EstadoFacade;
    private List<Estado> listaEstado;
    private Estado Estado;
    String mensaje="";

    public List<Estado> getListaEstado() {
        this.listaEstado= this.EstadoFacade.findAll();
        return listaEstado;
    }

    public void setListaEstado(List<Estado> listaEstado) {
        this.listaEstado = listaEstado;
    }

    public Estado getEstado() {
        return Estado;
    }

    public void setEstado(Estado Estado) {
        this.Estado = Estado;
    }
    
    @PostConstruct   
    public void init(){
        this.Estado = new Estado();
    }
    
    public void guardar(){
        try{
            this.EstadoFacade.create(Estado);
            this.mensaje="Se guardo el Estado Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (Estado e){
        this.Estado = e;
    }
    
    public void editar(){
        try{
            this.EstadoFacade.edit(Estado);
            this.mensaje="Se modifico el Estado Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void eliminar(Estado es){
        try{
            this.EstadoFacade.remove(es);
            this.mensaje="Se elimino el Estado Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        this.Estado = new Estado();
    }
}