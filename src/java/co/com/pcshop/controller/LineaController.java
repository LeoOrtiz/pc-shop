package co.com.pcshop.controller;

import co.com.pcshop.entities.Linea;
import co.com.pcshop.facade.local.LineaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LineaController implements Serializable {
    @EJB
    private LineaFacadeLocal LineaFacade;
    private List<Linea> listaLinea;
    private Linea Linea;
    String mensaje="";

    public List<Linea> getListaLinea() {
        this.listaLinea= this.LineaFacade.findAll();
        return listaLinea;
    }

    public void setListaLinea(List<Linea> listaLinea) {
        this.listaLinea = listaLinea;
    }

    public Linea getLinea() {
        return Linea;
    }

    public void setLinea(Linea Linea) {
        this.Linea = Linea;
    }
    
    @PostConstruct   
    public void init(){
        this.Linea = new Linea();
    }
    
    public void guardar(){
        try{
            this.LineaFacade.create(Linea);
            Linea = new Linea();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se guardo la Linea satisfactoriamente."));
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (Linea l){
        this.Linea = l;
    }
    
    public void editar(){
        try{
            this.LineaFacade.edit(Linea);
            this.mensaje="Se modifico la Linea Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void eliminar(Linea l){
        try{
            this.LineaFacade.remove(l);
            this.mensaje="Se elimino la Linea Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        this.Linea = new Linea();
    }
}