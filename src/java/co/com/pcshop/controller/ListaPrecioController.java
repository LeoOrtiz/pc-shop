package co.com.pcshop.controller;

import co.com.pcshop.entities.ListaPrecio;
import co.com.pcshop.facade.local.ListaPrecioFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class ListaPrecioController implements Serializable {
    @EJB
    private ListaPrecioFacadeLocal ListaPrecioFacade;
    private List<ListaPrecio> listaListaPrecio;
    private ListaPrecio ListaPrecio;
    String mensaje="";

    public List<ListaPrecio> getListaListaPrecio() {
        this.listaListaPrecio= this.ListaPrecioFacade.findAll();
        return listaListaPrecio;
    }

    public void setListaListaPrecio(List<ListaPrecio> listaListaPrecioo) {
        this.listaListaPrecio = listaListaPrecio;
    }

    public ListaPrecio getListaPrecio() {
        return ListaPrecio;
    }

    public void setListaPrecio(ListaPrecio ListaPrecio) {
        this.ListaPrecio = ListaPrecio;
    }
    
    @PostConstruct   
    public void init(){
        this.ListaPrecio = new ListaPrecio();
    }
    
    public void guardar(){
        try{
            this.ListaPrecioFacade.create(ListaPrecio);
            this.mensaje="Se guardo la Lista de Precio Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (ListaPrecio lp){
        this.ListaPrecio = lp;
    }
    
    public void editar(){
        try{
            this.ListaPrecioFacade.edit(ListaPrecio);
            this.mensaje="Se modifico la Lista de Precio Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void eliminar(ListaPrecio lp){
        try{
            this.ListaPrecioFacade.remove(lp);
            this.mensaje="Se elimino la Lista de Precio Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        this.ListaPrecio = new ListaPrecio();
    }
}