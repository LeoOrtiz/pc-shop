package co.com.pcshop.controller;

import co.com.pcshop.entities.Marca;
import co.com.pcshop.facade.local.MarcaFacadeLocal;
import co.com.pcshop.util.Constantes;
import co.com.pcshop.util.UtilPath;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

@ManagedBean
@SessionScoped
public class MarcaController implements Serializable {

    @EJB
    private MarcaFacadeLocal MarcaFacade;
    private List<Marca> listaMarca;
    private Marca marca;
    String mensaje = "";
    private UploadedFile file;
    String destination;
    String ImageName = "";
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<Marca> getListaMarca() {
        this.listaMarca = this.MarcaFacade.findAll();
        return listaMarca;
    }

    public void setListaMarca(List<Marca> listaMarca) {
        this.listaMarca = listaMarca;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca Marca) {
        this.marca = Marca;
    }

    @PostConstruct
    public void init() {
        this.marca = new Marca();
    }

    public void cargaId(Marca m) {
        this.marca = m;
    }

    public void eliminar(Marca m) {
        try {
            this.MarcaFacade.remove(m);
            this.mensaje = "Se elimino la Marca Satisfactoriamente!";
        } catch (Exception e) {
            this.mensaje = "Error: " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void limpiar() {
        this.marca = new Marca();
    } 
    
    
    public void guardar() {
        try{
            if (upload()) {
                marca.setMarFoto(ImageName);
                this.MarcaFacade.create(marca);
                this.mensaje = "Se guardo la Marca Satisfactoriamente!";
                this.marca = new Marca();
                System.out.println("this = " + marca.getMarId());
                destination = "";
                System.out.println("destination = " + destination);
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void editar() {
        try{
            if (upload()) {
                marca.setMarFoto(ImageName);
                this.MarcaFacade.edit(marca);
                this.mensaje = "Se modifico la Marca Satisfactoriamenteee!";
                this.marca = new Marca();
                destination = "";
                System.out.println("destination = " + destination);
            }else{
                this.MarcaFacade.edit(marca);
                this.mensaje = "Se modifico la Marca Satisfactoriamenteee!";
                this.marca = new Marca();
                destination = "";
                System.out.println("destination = " + destination);
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public Boolean upload() {
        try {
            destination = Constantes.URL;
            System.out.println("UPLOAD destination = " + destination);
            File folder = new File(UtilPath.getPath() + destination);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            copyFile(getFile().getFileName(), getFile().getInputstream());
            if("".equals(getFile().getFileName())){
                return false;
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void copyFile(String fileName, InputStream in) {
        try {
            ImageName=fileName;
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            String realPath = UtilPath.getPathDefinida(ec.getRealPath("/"));
            destination = destination + Constantes.SEPARADOR + fileName;
            System.out.println("COPYFILE destination = " + destination);
            OutputStream out = new FileOutputStream(new File(realPath + destination));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            //JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("error_cargar_doc"));
        }
    }
}
