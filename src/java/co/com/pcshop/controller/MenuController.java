package co.com.pcshop.controller;

import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.TipoUsuario;
import co.com.pcshop.entities.Usuario;
import co.com.pcshop.facade.local.MenuFacadeLocal;
import co.com.pcshop.facade.local.TipoUsuarioFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

@Named
@SessionScoped
public class MenuController implements Serializable {

    @EJB
    private MenuFacadeLocal menuEJB;
    @EJB
    private TipoUsuarioFacadeLocal tipousuarioEJB;

    private List<Menu> lista;
    private List<Menu> lista_menugeneral;

    private Menu menu;

    private int idtipousuario;
    private int idsubmenu;
    private String idmenu;

    List<Menu> listamenu = null;
    String mensaje = " ";

    private MenuModel model;

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Menu> getLista() {
        return lista;
    }

    public void setLista(List<Menu> lista) {
        this.lista = lista;
    }

    public int getIdtipousuario() {
        return idtipousuario;
    }

    public void setIdtipousuario(int idtipousuario) {
        this.idtipousuario = idtipousuario;
    }

    public int getIdsubmenu() {
        return idsubmenu;
    }

    public void setIdsubmenu(int idsubmenu) {
        this.idsubmenu = idsubmenu;
    }

    public List<Menu> getListamenu() {
        if (listamenu == null) {
            listamenu = menuEJB.findAll();
        }
        listamenu = menuEJB.findAll();
        return listamenu;
    }

    public void setListamenu(List<Menu> listamenu) {
        this.listamenu = listamenu;
    }

    public List<Menu> getListamenugeneral() {
        if (lista_menugeneral == null) {
            lista_menugeneral = menuEJB.Menu_xTipo();
        }
        lista_menugeneral = menuEJB.Menu_xTipo();
        return lista_menugeneral;
    }

    public void setListamenugeneral(List<Menu> listamenu) {
        this.listamenu = listamenu;
    }

    public String getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(String idmenu) {
        this.idmenu = idmenu;
    }

    @PostConstruct
    public void init() {
        menu = new Menu();
        this.ListarMenus();
        model = new DefaultMenuModel();
        this.EstablecerPermisos();
    }

    public void ListarMenus() {
        try {
            lista = menuEJB.findAll();
        } catch (Exception e) {

        }
    }

    public void EstablecerPermisos() {
        Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        //LLENA LOS MENUS QUE TIENEN SUBMENUS
        for (Menu m : lista) {
            //SI EL TIPO DE MENU = S Y EL MENU TIPO USUARIO = USUARIO TIPOUSUARIO
            if ((m.getMenuTipo()).equals("S") && m.getMenuTipoUsuario().equals(us.getUsuTipoUsuario())) {
                DefaultSubMenu firstSubmenu = new DefaultSubMenu(m.getMenNombre());
                for (Menu i : lista) {
                    Menu submenu = i.getMenCodigoSubmenu();
                    if (submenu != null) {
                        if (submenu.getMenCodigo() == m.getMenCodigo()) {
                            DefaultMenuItem item = new DefaultMenuItem(i.getMenNombre());
                            item.setUrl(i.getMenUrl());
                            firstSubmenu.addElement(item);
                        }
                    }
                }
//                for (Menu i : lista) {
//                    Menu submenu = i.getMenCodigoSubmenu();
//                    if (submenu != null) {
//                        if (submenu.getMenCodigo() == m.getMenCodigo()) {
//                            DefaultMenuItem item = new DefaultMenuItem(i.getMenNombre());
//                            item.setUrl(i.getMenUrl());
//                            firstSubmenu.addElement(item);
//                        }
//                    }
//                }
                model.addElement(firstSubmenu);
            } else {
                //LLENA SOLO LOS MENUS
                if (m.getMenCodigoSubmenu() == null && m.getMenuTipoUsuario().equals(us.getUsuTipoUsuario())) {
                    DefaultMenuItem item = new DefaultMenuItem(m.getMenNombre());
                    item.setUrl(m.getMenUrl());
                    model.addElement(item);
                }
            }
        }
    }

    public void guardar() {
        try {
            Menu menus = menuEJB.ultimoMenu();
            //SE OBTIENE EL ULTIMO id DE LOS MENUS
            int id_menu = menus.getMenCodigo();

            switch (idtipousuario) {
                case 0:
                    if (idsubmenu > 0) {
                        List<TipoUsuario> lista_tipousuarios = tipousuarioEJB.findAll();
                        for (TipoUsuario t : lista_tipousuarios) {
                            id_menu = id_menu + 1;
                            menu.setMenCodigo(id_menu);
                            menu.setMenuTipo(idmenu);
                            menu.setMenCodigoSubmenu(new Menu(idsubmenu));
                            menu.setMenuTipoUsuario(new TipoUsuario(t.getTipusuId()));
                            menuEJB.create(menu);
                            this.mensaje = "Registrado Correctamente";
                        }
                        menu = new Menu();
                    }else{
                        List<TipoUsuario> lista_tipousuarios = tipousuarioEJB.findAll();
                        for (TipoUsuario t : lista_tipousuarios) {
                            id_menu = id_menu + 1;
                            menu.setMenCodigo(id_menu);
                            menu.setMenuTipo(idmenu);
                            menu.setMenuTipoUsuario(new TipoUsuario(t.getTipusuId()));
                            menuEJB.create(menu);
                            this.mensaje = "Registrado Correctamente";
                        }
                        menu = new Menu();
                    }
                    
                    break;
                default:
                    if(idsubmenu>0){
                        menu.setMenuTipo(idmenu);
                        menu.setMenCodigoSubmenu(new Menu(idsubmenu));
                        menu.setMenuTipoUsuario(new TipoUsuario(idtipousuario));
                        menuEJB.create(menu);
                        menu = new Menu();
                        this.mensaje = "Registrado Correctamente";

                    }else{
                        menu.setMenuTipo(idmenu);
                        menu.setMenuTipoUsuario(new TipoUsuario(idtipousuario));
                        menuEJB.create(menu);
                        menu = new Menu();
                        this.mensaje = "Registrado Correctamente";
                    }
                    break;
            }
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            menu = new Menu();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        menu = new Menu();
        listamenu = menuEJB.findAll();
    }

//  
    public void cargarId(Menu m) {
        this.menu = m;
    }

    public void editar() {
        try {
            menu.setMenuTipo(idmenu);
            menu.setMenCodigoSubmenu(new Menu(idsubmenu));
            menu.setMenuTipoUsuario(new TipoUsuario(idtipousuario));
            menuEJB.edit(menu);
            this.mensaje = " editado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listamenu = menuEJB.findAll();
    }

    public void limpiar() {
        this.menu = new Menu();
    }

    public void eliminar(Menu m) {
        try {
            menuEJB.remove(m);
            this.mensaje = "eliminado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listamenu = menuEJB.findAll();
    }
}
