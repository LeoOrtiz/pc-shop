package co.com.pcshop.controller;

import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.Persona;
import co.com.pcshop.facade.local.PersonaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author mauricio-veronica
 */
@ManagedBean
@SessionScoped
public class PersonaController implements Serializable {

    @EJB
    private PersonaFacadeLocal personaEJB;
    
    //SE CREAN LOS OBJECTOS
    private Persona persona;
    
    List<Persona> listapersona = null;
    List<Menu> listamenu = null;
    
    String mensaje = " ";

    
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public List<Persona> getListapersona() {

        if (listapersona == null) {
            listapersona = personaEJB.findAll();
        }
        listapersona = personaEJB.findAll();
        return listapersona;
    }

    public void setListapersona(List<Persona> listapersona) {
        this.listapersona = listapersona;
    }

    
    
    @PostConstruct
    public void init() {
        persona = new Persona();//genera una instancia al objeto

    }

    public void guardar() {
        try {
            personaEJB.create(persona);
            persona = new Persona();
            this.mensaje = "Registrado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            persona = new Persona();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        persona = new Persona();
        listapersona = personaEJB.findAll();
    }

//  
    public void cargarId(Persona p) {
        this.persona = p;
    }

    public void editar() {
        try {

            personaEJB.edit(persona);
            this.mensaje = " editado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listapersona = personaEJB.findAll();
    }

    public void limpiar() {
        this.persona = new Persona();
    }

    public void eliminar(Persona p) {
        try {
            personaEJB.remove(p);
            this.mensaje = "eliminado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listapersona = personaEJB.findAll();
    }

}