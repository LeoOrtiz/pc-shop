package co.com.pcshop.controller;

import co.com.pcshop.entities.Estado;
import co.com.pcshop.entities.Proveedor;
import co.com.pcshop.entities.TipoDocumento;
import co.com.pcshop.facade.local.ProveedorFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author mauricio-veronica
 */
@ManagedBean
@SessionScoped
public class ProveedorController implements Serializable {

    @EJB
    private ProveedorFacadeLocal proveedorEJB;
    
    //SE CREAN LOS OBJECTOS
    private Proveedor proveedor;
    private TipoDocumento tipodocumento;
    private Estado estado;
    
    //RECUPERA LOS VALORES DE LOS h:selectOneMenu DE LA VISTA
    private int idtipodocumento;
    private int idestado;
    
    List<Proveedor> listaproveedor = null;
    
    String mensaje = " ";

    
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public TipoDocumento getTipoDocumento() {
        return tipodocumento;
    }

    public void setTipoDocumento(TipoDocumento tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public int getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(int idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }
    
    public List<Proveedor> getListaproveedor() {

        if (listaproveedor == null) {
            listaproveedor = proveedorEJB.findAll();
        }
        listaproveedor = proveedorEJB.findAll();
        return listaproveedor;
    }

    public void setListaproveedor(List<Proveedor> listaproveedor) {
        this.listaproveedor = listaproveedor;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public int getIdestado() {
        return idestado;
    }

    public void setIdestado(int idestado) {
        this.idestado = idestado;
    }

    
    
    @PostConstruct
    public void init() {
        proveedor = new Proveedor();//genera una instancia al objeto

    }

    public void guardar() {
        try {
            proveedor.setProTipoDocumento(new TipoDocumento(idtipodocumento));
            proveedor.setProEstado(new Estado(idestado));
            proveedorEJB.create(proveedor);
            proveedor = new Proveedor();
            this.mensaje = "Registrado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            proveedor = new Proveedor();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        proveedor = new Proveedor();
        listaproveedor = proveedorEJB.findAll();
    }

//  
    public void cargarId(Proveedor p) {
        this.proveedor = p;
    }

    public void editar() {
        try {

            proveedor.setProTipoDocumento(new TipoDocumento(idtipodocumento));
            proveedor.setProEstado(new Estado(idestado));
            proveedorEJB.edit(proveedor);
            this.mensaje = " editado Correctamente";
            proveedor = new Proveedor();
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listaproveedor = proveedorEJB.findAll();
    }

    public void limpiar() {
        this.proveedor = new Proveedor();
    }

    public void eliminar(Proveedor p) {
        try {
            proveedorEJB.remove(p);
            this.mensaje = "eliminado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listaproveedor = proveedorEJB.findAll();
    }

}