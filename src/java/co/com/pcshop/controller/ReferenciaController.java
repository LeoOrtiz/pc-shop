package co.com.pcshop.controller;

import co.com.pcshop.entities.Linea;
import co.com.pcshop.entities.Marca;
import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.facade.local.ReferenciaFacadeLocal;
import co.com.pcshop.util.Constantes;
import co.com.pcshop.util.UtilPath;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.chart.PieChartModel;


@Named
@ViewScoped
public class ReferenciaController implements Serializable { 
    String mensaje="";
    
    @EJB
    private ReferenciaFacadeLocal referenciaEJB;
    
    private Referencia referencia;
    private Linea linea;
    private Marca marca;
    
    private int idlinea;
    private int idmarca;
    
    List<Referencia> listareferencia = null;
    
    private UploadedFile file;
    String destination;
    String ImageName = "";
    
    @PostConstruct
    public void init(){
        referencia = new Referencia();
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }
    
    public List<Referencia> getListareferencia() {

        if (listareferencia == null) {
            listareferencia = referenciaEJB.findAll();
        }
        listareferencia = referenciaEJB.findAll();
        return listareferencia;
    }

    public void setListareferencia(List<Referencia> listareferencia) {
        this.listareferencia = listareferencia;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Linea getLinea() {
        return linea;
    }

    public void setLinea(Linea linea) {
        this.linea = linea;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public int getIdlinea() {
        return idlinea;
    }

    public void setIdlinea(int idlinea) {
        this.idlinea = idlinea;
    }

    public int getIdmarca() {
        return idmarca;
    }

    public void setIdmarca(int idmarca) {
        this.idmarca = idmarca;
    }
    
    
    
    public void guardar() {
        try{
            if (upload()) {
                referencia.setRefFoto(ImageName);
                referencia.setRefLinea(new Linea(idlinea));
                referencia.setRefMarca(new Marca(idmarca));
                referenciaEJB.create(referencia);
                referencia = new Referencia();
                mensaje="Se guardo la Referencia Satisfactoriamente!";
                destination = "";
                System.out.println("destination = " + destination);
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (Referencia referencia){
        this.referencia = referencia;
    }
    
    public void editar(){
        try{
            if (upload()) {
                referencia.setRefFoto(ImageName);
                referencia.setRefLinea(new Linea(idlinea));
                referencia.setRefMarca(new Marca(idmarca));
                referenciaEJB.edit(referencia);
                mensaje="Se modifico la Referencia Satisfactoriamente!";
                this.referencia = new Referencia();
                destination = "";
                System.out.println("destination = " + destination);
            }else{
                referencia.setRefLinea(new Linea(idlinea));
                referencia.setRefMarca(new Marca(idmarca));
                referenciaEJB.edit(referencia);
                mensaje = "Se modifico la Referencia Satisfactoriamente!";
                referencia = new Referencia();
                destination = "";
                System.out.println("destination = " + destination);
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public Boolean upload() {
        try {
            System.out.println("uload = ");
            destination = Constantes.URL_REFERENCIAS;
            System.out.println("UPLOAD destination = " + destination);
            File folder = new File(UtilPath.getPath() + destination);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            copyFile(getFile().getFileName(), getFile().getInputstream());
            if("".equals(getFile().getFileName())){
                return false;
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void copyFile(String fileName, InputStream in) {
        try {
            System.out.println("copyfile = ");
            ImageName=fileName;
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            String realPath = UtilPath.getPathDefinida(ec.getRealPath("/"));
            destination = destination + Constantes.SEPARADOR + fileName;
            System.out.println("COPYFILE destination = " + destination);
            OutputStream out = new FileOutputStream(new File(realPath + destination));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            //JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("error_cargar_doc"));
        }
    }
    
    public void eliminar(Referencia u){
        try{
            referenciaEJB.remove(u);
            referencia = new Referencia();
            referencia.setRefLinea(new Linea());
            referencia.setRefMarca(new Marca());
            mensaje="Se elimino la Referencia Satisfactoriamente!";
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        referencia = new Referencia();
        referencia.setRefLinea(new Linea());
        referencia.setRefMarca(new Marca());
    }
    
    
}