package co.com.pcshop.controller;

import co.com.pcshop.entities.Estado;
import co.com.pcshop.entities.ReferenciaListaPrecio;
import co.com.pcshop.entities.TipoDocumento;
import co.com.pcshop.facade.local.ReferenciaListaPrecioFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author mauricio-veronica
 */
@ManagedBean
@SessionScoped
public class ReferenciaListaPrecioController implements Serializable {

    @EJB
    private ReferenciaListaPrecioFacadeLocal reflispreEJB;
    
    //SE CREAN LOS OBJECTOS
    private ReferenciaListaPrecio reflispre;
    
    List<ReferenciaListaPrecio> listareflispre = null;
    
    String mensaje = " ";

    public ReferenciaListaPrecio getReflispre() {
        return reflispre;
    }

    public void setReflispre(ReferenciaListaPrecio reflispre) {
        this.reflispre = reflispre;
    }

    public List<ReferenciaListaPrecio> getListareflispre() {

        if (listareflispre == null) {
            listareflispre = reflispreEJB.findAll();
        }
        listareflispre = reflispreEJB.findAll();
        return listareflispre;
    }

    public void setListareflispre(List<ReferenciaListaPrecio> listareflispre) {
        this.listareflispre = listareflispre;
    }
    
    @PostConstruct
    public void init() {
        reflispre = new ReferenciaListaPrecio();//genera una instancia al objeto

    }
    
    public void cargarId(ReferenciaListaPrecio p) {
        this.reflispre = p;
    }

    public void editar() {
        try {
            reflispreEJB.edit(reflispre);
            this.mensaje = " editado Correctamente";
            reflispre = new ReferenciaListaPrecio();
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listareflispre = reflispreEJB.findAll();
    }

    public void limpiar() {
        this.reflispre = new ReferenciaListaPrecio();
    }
}