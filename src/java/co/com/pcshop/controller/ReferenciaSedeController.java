package co.com.pcshop.controller;

import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.ReferenciaSede;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.facade.local.ReferenciaSedeFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ReferenciaSedeController implements Serializable {

    @EJB
    private ReferenciaSedeFacadeLocal refsedEJB;
    
    //SE CREAN LOS OBJECTOS
    private ReferenciaSede refsed;
    private Referencia referencia;
    private Sede sede;
    
    List<ReferenciaSede> listarefsed = null;
    
    String mensaje = " ";

    public ReferenciaSede getReflispre() {
        return refsed;
    }

    public void setReflispre(ReferenciaSede refsed) {
        this.refsed = refsed;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    
    
    public List<ReferenciaSede> getListarefsed() {

        if (listarefsed == null) {
            listarefsed = refsedEJB.findAll();
        }
        listarefsed = refsedEJB.findAll();
        return listarefsed;
    }

    public void setListarefsed(List<ReferenciaSede> listarefsed) {
        this.listarefsed = listarefsed;
    }
    
    @PostConstruct
    public void init() {
        refsed = new ReferenciaSede();//genera una instancia al objeto
    }
    
    public void cargarId(ReferenciaSede p) {
        this.refsed = p;
    }

    public void limpiar() {
        this.refsed = new ReferenciaSede();
    }
}