package co.com.pcshop.controller;

import co.com.pcshop.entities.Empresa;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Ubicacion;
import co.com.pcshop.facade.local.SedeFacadeLocal;
import co.com.pcshop.util.Constantes;
import co.com.pcshop.util.UtilPath;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

@ManagedBean
@SessionScoped
public class SedeController implements Serializable {

    @EJB
    private SedeFacadeLocal SedeFacade;
    
    private List<Sede> listaSede;
    
    private Sede sede;
    private Empresa empresa;
    private Ubicacion ubicacion;
    
    private int idempresa;
    private int idubicacion;
    
    
    String mensaje = "";
    private UploadedFile file;
    String destination;
    String ImageName = "";
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<Sede> getListaSede() {
        this.listaSede = this.SedeFacade.findAll();
        return listaSede;
    }

    public void setListaSede(List<Sede> listaSede) {
        this.listaSede = listaSede;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede Sede) {
        this.sede = Sede;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public int getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(int idempresa) {
        this.idempresa = idempresa;
    }

    public int getIdubicacion() {
        return idubicacion;
    }

    public void setIdubicacion(int idubicacion) {
        this.idubicacion = idubicacion;
    }

    
    
    
    
    @PostConstruct
    public void init() {
        this.sede = new Sede();
    }

    public void cargaId(Sede m) {
        this.sede = m;
    }

    public void eliminar(Sede m) {
        try {
            this.SedeFacade.remove(m);
            this.mensaje = "Se elimino la Sede Satisfactoriamente!";
        } catch (Exception e) {
            this.mensaje = "Error: " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void limpiar() {
        this.sede = new Sede();
    } 
    
    
    public void guardar() {
        try{
            if (upload()) {
                sede.setSedFoto(ImageName);
                sede.setSedEmpresa(new Empresa(idempresa));
                sede.setSedUbicacion(new Ubicacion(idubicacion));
                this.SedeFacade.create(sede);
                this.mensaje = "Se guardo la Sede Satisfactoriamente!";
                this.sede = new Sede();
                System.out.println("this = " + sede.getSedId());
                destination = "";
                System.out.println("destination = " + destination);
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void editar() {
        try{
            if (upload()) {
                sede.setSedFoto(ImageName);
                sede.setSedEmpresa(new Empresa(idempresa));
                sede.setSedUbicacion(new Ubicacion(idubicacion));
                this.SedeFacade.edit(sede);
                this.mensaje = "Se modifico la Sede Satisfactoriamenteee!";
                this.sede = new Sede();
                destination = "";
                System.out.println("destination = " + destination);
            }else{
                sede.setSedEmpresa(new Empresa(idempresa));
                sede.setSedUbicacion(new Ubicacion(idubicacion));
                this.SedeFacade.edit(sede);
                this.mensaje = "Se modifico la Sede Satisfactoriamenteee!";
                this.sede = new Sede();
                destination = "";
                System.out.println("destination = " + destination);
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public Boolean upload() {
        try {
            destination = Constantes.URL_SEDES;
            System.out.println("UPLOAD destination = " + destination);
            File folder = new File(UtilPath.getPath() + destination);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            copyFile(getFile().getFileName(), getFile().getInputstream());
            if("".equals(getFile().getFileName())){
                return false;
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void copyFile(String fileName, InputStream in) {
        try {
            ImageName=fileName;
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            String realPath = UtilPath.getPathDefinida(ec.getRealPath("/"));
            destination = destination + Constantes.SEPARADOR + fileName;
            System.out.println("COPYFILE destination = " + destination);
            OutputStream out = new FileOutputStream(new File(realPath + destination));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            //JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("error_cargar_doc"));
        }
    }
}
