package co.com.pcshop.controller;

import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.TipoDocumento;
import co.com.pcshop.facade.local.TipoDocumentoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author mauricio-veronica
 */
@ManagedBean
@SessionScoped
public class TipoDocumentoController implements Serializable {

    @EJB
    private TipoDocumentoFacadeLocal tipodocumentoEJB;
    
    //SE CREAN LOS OBJECTOS
    private TipoDocumento tipodocumento;
    
    List<TipoDocumento> listatipodocumento = null;
    
    String mensaje = " ";

    
    public TipoDocumento getTipoDocumento() {
        return tipodocumento;
    }

    public void setTipoDocumento(TipoDocumento tipodocumento) {
        this.tipodocumento = tipodocumento;
    }
    
    public List<TipoDocumento> getListatipodocumento() {

        if (listatipodocumento == null) {
            listatipodocumento = tipodocumentoEJB.findAll();
        }
        listatipodocumento = tipodocumentoEJB.findAll();
        return listatipodocumento;
    }

    public void setListatipodocumento(List<TipoDocumento> listatipodocumento) {
        this.listatipodocumento = listatipodocumento;
    }

    
    
    @PostConstruct
    public void init() {
        tipodocumento = new TipoDocumento();//genera una instancia al objeto

    }
}