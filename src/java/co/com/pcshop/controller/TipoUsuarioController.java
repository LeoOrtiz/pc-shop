package co.com.pcshop.controller;

import co.com.pcshop.entities.TipoUsuario;
import co.com.pcshop.facade.local.TipoUsuarioFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class TipoUsuarioController implements Serializable {
    @EJB
    private TipoUsuarioFacadeLocal TipoUsuarioFacade;
    private List<TipoUsuario> listaTipousuario;
    private TipoUsuario Tipousuario;
    String mensaje="";

    public List<TipoUsuario> getListaTipousuario() {
        this.listaTipousuario= this.TipoUsuarioFacade.findAll();
        return listaTipousuario;
    }

    public void setListaTipousuario(List<TipoUsuario> listaTipousuario) {
        this.listaTipousuario = listaTipousuario;
    }

    public TipoUsuario getTipousuario() {
        return Tipousuario;
    }

    public void setTipousuario(TipoUsuario Tipousuario) {
        this.Tipousuario = Tipousuario;
    }
    
    @PostConstruct   
    public void init(){
        this.Tipousuario = new TipoUsuario();
    }
    
    public void guardar(){
        try{
            this.TipoUsuarioFacade.create(Tipousuario);
            this.mensaje="Se guardo el Tipo de Usuario Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (TipoUsuario t){
        this.Tipousuario = t;
    }
    
    public void editar(){
        try{
            this.TipoUsuarioFacade.edit(Tipousuario);
            this.mensaje="Se modifico el Tipo de Usuario Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void eliminar(TipoUsuario t){
        try{
            this.TipoUsuarioFacade.remove(t);
            this.mensaje="Se elimino el Tipo de Usuario Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        this.Tipousuario = new TipoUsuario();
    }
}