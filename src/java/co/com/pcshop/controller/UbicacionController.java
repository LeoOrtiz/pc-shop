package co.com.pcshop.controller;

import co.com.pcshop.entities.Ubicacion;
import co.com.pcshop.facade.local.UbicacionFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class UbicacionController implements Serializable {
    @EJB
    private UbicacionFacadeLocal UbicacionFacade;
    private List<Ubicacion> listaUbicacion;
    private Ubicacion Ubicacion;
    String mensaje="";

    public List<Ubicacion> getListaUbicacion() {
        this.listaUbicacion= this.UbicacionFacade.findAll();
        return listaUbicacion;
    }

    public void setListaUbicacion(List<Ubicacion> listaUbicacion) {
        this.listaUbicacion = listaUbicacion;
    }

    public Ubicacion getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(Ubicacion Ubicacion) {
        this.Ubicacion = Ubicacion;
    }
    
    @PostConstruct   
    public void init(){
        this.Ubicacion = new Ubicacion();
    }
    
    public void guardar(){
        try{
            this.UbicacionFacade.create(Ubicacion);
            this.mensaje="Se guardo la Ubicacion Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (Ubicacion u){
        this.Ubicacion = u;
    }
    
    public void editar(){
        try{
            this.UbicacionFacade.edit(Ubicacion);
            this.mensaje="Se modifico la Ubicacion Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void eliminar(Ubicacion u){
        try{
            this.UbicacionFacade.remove(u);
            this.mensaje="Se elimino la Ubicacion Satisfactoriamente!";
        }catch (Exception e){
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        this.Ubicacion = new Ubicacion();
    }
}