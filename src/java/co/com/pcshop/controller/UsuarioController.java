package co.com.pcshop.controller;

import co.com.pcshop.entities.Estado;
import co.com.pcshop.entities.ListaPrecio;
import co.com.pcshop.entities.Menu;
import co.com.pcshop.entities.Persona;
import co.com.pcshop.entities.TipoUsuario;
import co.com.pcshop.entities.Usuario;
import co.com.pcshop.facade.local.UsuarioFacadeLocal;
import co.com.pcshop.facade.local.MenuFacadeLocal;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author mauricio-veronica
 */
@ManagedBean
@SessionScoped
public class UsuarioController implements Serializable {

    @EJB
    private UsuarioFacadeLocal ejbFacade;
    @EJB
    private MenuFacadeLocal menuFacade;
    
    //SE CREAN LOS OBJECTOS
    private Usuario usuario;
    private TipoUsuario tipousuario;
    private Persona persona;
    private Estado estado;
    private ListaPrecio listaprecio;
    
    //RECUPERA LOS VALORES DE LOS h:selectOneMenu DE LA VISTA
    private int idpersona;
    private int idtipousuario;
    private int idestado;
    private int idlistaprecio;
    
    List<Usuario> listausuario = null;
    List<Menu> listamenu = null;
    
    String mensaje = " ";
    
    private String accion;
    private String user;
    private String password;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public int getIdestado() {
        return idestado;
    }

    public void setIdestado(int idestado) {
        this.idestado = idestado;
    }
    
    public TipoUsuario getTipousuario() {
        return tipousuario;
    }

    public void setTipousuario(TipoUsuario tipousuario) {
        this.tipousuario = tipousuario;
    }

    public int getIdtipousuario() {
        return idtipousuario;
    }

    public void setIdtipousuario(int idtipousuario) {
        this.idtipousuario = idtipousuario;
    }

    public UsuarioFacadeLocal getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(UsuarioFacadeLocal ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getListausuario() {

        if (listausuario == null) {
            listausuario = ejbFacade.findAll();
        }
        listausuario = ejbFacade.findAll();
        return listausuario;
    }

    public void setListausuario(List<Usuario> listausuario) {
        this.listausuario = listausuario;
    }

    public ListaPrecio getListaprecio() {
        return listaprecio;
    }

    public void setListaprecio(ListaPrecio listaprecio) {
        this.listaprecio = listaprecio;
    }

    public int getIdlistaprecio() {
        return idlistaprecio;
    }

    public void setIdlistaprecio(int idlistaprecio) {
        this.idlistaprecio = idlistaprecio;
    }

    
    
    @PostConstruct
    public void init() {
        usuario = new Usuario();//genera una instancia al objeto

    }

    public void guardar() {
        try {
            usuario.setUsuPersona(new Persona(idpersona));
            usuario.setUsuTipoUsuario(new TipoUsuario(idtipousuario));
            usuario.setUsuEstado(new Estado(idestado));
            usuario.setUsuListaPrecio(new ListaPrecio(idlistaprecio));
            
            String encript = DigestUtils.sha1Hex(this.usuario.getUsuClave());
            this.usuario.setUsuClave(encript);
            ejbFacade.create(usuario);

            usuario = new Usuario();

            this.mensaje = "Registrado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            usuario = new Usuario();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        usuario = new Usuario();
        listausuario = ejbFacade.findAll();
    }

//  
    public void cargarId(Usuario t) {
        this.usuario = t;
    }

    public void editar() {
        try {

            usuario.setUsuPersona(new Persona(idpersona));
            usuario.setUsuTipoUsuario(new TipoUsuario(idtipousuario));
            usuario.setUsuEstado(new Estado(idestado));
            usuario.setUsuListaPrecio(new ListaPrecio(idlistaprecio));
            
            String encript = DigestUtils.sha1Hex(this.usuario.getUsuClave());
            this.usuario.setUsuClave(encript);
            ejbFacade.edit(usuario);
            this.mensaje = " editado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listausuario = ejbFacade.findAll();
    }

    public void limpiar() {
        this.usuario = new Usuario();
    }

    public void eliminar(Usuario tp) {
        try {
            ejbFacade.remove(tp);
            this.mensaje = "eliminado Correctamente";
        } catch (Exception e) {
            this.mensaje = "ERROR :" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
        listausuario = ejbFacade.findAll();
    }


    public void cerrarSesion() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

    }


    public String verificar() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        usuario = ejbFacade.iniciarSesion(user);
        if (usuario != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
            
            String encriptado = DigestUtils.sha1Hex(password);
            if (usuario.getUsuClave().equals(encriptado)) {
                if(usuario.getUsuEstado().getEstId()==20){
                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
                    session.setAttribute("idUsuario", usuario.getUsuId());
                    TipoUsuario tipouser = usuario.getUsuTipoUsuario();
                    session.setAttribute("idtipousuario", tipouser.getTipusuId());
                    return "template";
                }else{
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Aviso!", "El usuario no se encuentra Activo"));
                    return null;
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso!", "La contraseña es incorrecta no corresponde"));
                return null;
            }
            

        }
        context.getExternalContext().redirect("/PC-SHOP/faces/login.xhtml");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso!", "El usuario no existe"));
        return null;
    }

    public void VerificarSesion() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
            if (us == null) {
                context.getExternalContext().redirect("/PC-SHOP/faces/login.xhtml");
            }
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            //SE GUARDA LA RUTA EN LA QUE SE ENCUENTRA EL USUARIO
            String url = request.getRequestURI();
            
            //SE ELIMINAN LOS PRIMEROS 8 CARACTERES DE LA RUTA (/PC-SHOP)
            String url_modificada=url.substring(8);
            int permiso=0;
            
            //EVITA QUE AL INICIAR SESION 
            String login="/faces/login.xhtml";
            if (login.equals(url_modificada)){
                //SE LE ASIGNA UN 1 A LA VARIABLE url_false
                permiso=permiso+1;
            }else {
                //SE ALMACENAN TODOS LOS MENUS EN listamenu
                listamenu = menuFacade.findAll();
                for (Menu m : listamenu){
                    //SI EL TIPO DE USUARIO DEL MENU ES IGUAL AL TIPO DE USUARIO LOGUEADO Y LA URL DEL MENU ES IGUAL A LA URL WEB
                    if(m.getMenuTipoUsuario().equals(us.getUsuTipoUsuario()) && m.getMenUrl().equals(url_modificada)){
                        //SE LE ASIGNA UN 1 A permiso
                        permiso=permiso+1;
                        break;
                    }else{
                        permiso=permiso-permiso;
                    }
                }
            }
            //SI NO SE ENCUENTRA LA URL DENTRO DE LA listamenu, permiso SEGUIRA SIENDO 0, POR LO TANTO NO TIENE PERMISO PARA ACCEDER 
            if(permiso==0){
                context.getExternalContext().redirect("/PC-SHOP/faces/login.xhtml");
                FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            }
        } catch (Exception e) {
       
        }
    }

}