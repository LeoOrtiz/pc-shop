package co.com.pcshop.controller;

import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.ReferenciaListaPrecio;
import co.com.pcshop.entities.Sede;
import co.com.pcshop.entities.Usuario;
import co.com.pcshop.entities.Venta;
import co.com.pcshop.facade.local.ReferenciaFacadeLocal;
import co.com.pcshop.facade.local.ReferenciaListaPrecioFacadeLocal;
import co.com.pcshop.facade.local.UsuarioFacadeLocal;
import co.com.pcshop.facade.local.VentaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


@Named
@ViewScoped
public class VentaController implements Serializable { 
    String mensaje="";
    
    @EJB
    private VentaFacadeLocal ventaEJB;
    @EJB
    private ReferenciaFacadeLocal referenciaEJB;
    @EJB
    private ReferenciaListaPrecioFacadeLocal reflispreEJB;
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    private Venta venta;
    private Usuario usuario;
    private Referencia referencia;
    private ReferenciaListaPrecio reflispre;
    private Sede sede;
    
    //RECUPERA LOS VALORES DE LOS h:selectOneMenu DE LA VISTA
    private int idreferencia;
    private int idlistaprecio;
    private int idsede;
    private int idusuario;
    
    
    //PARA VALIDAR LA CANTIDAD DE REFERENCIAS A VENDER
    private int VentasStock;
    
    //RECUPERA LOS VALORES DE LOS InputText
    private int cantidad;
    private double descuento;
    private double precio;
    
    List<Venta> listaventa = null;
    
    @PostConstruct
    public void init(){
        venta = new Venta();
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }
    
    public List<Venta> getListaventa() {

        if (listaventa == null) {
            listaventa = ventaEJB.findAll();
        }
        listaventa = ventaEJB.findAll();
        return listaventa;
    }

    public void setListaventa(List<Venta> listaventa) {
        this.listaventa = listaventa;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public int getIdreferencia() {
        return idreferencia;
    }

    public void setIdreferencia(int idreferencia) {
        this.idreferencia = idreferencia;
    }

    public int getIdlistaprecio() {
        return idlistaprecio;
    }

    public void setIdlistaprecio(int idlistaprecio) {
        this.idlistaprecio = idlistaprecio;
    }

    public int getVentasStock() {
        return VentasStock;
    }

    public void setVentasStock(int VentasStock) {
        this.VentasStock = VentasStock;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public int getIdsede() {
        return idsede;
    }

    public void setIdsede(int idsede) {
        this.idsede = idsede;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }
    
    
    
    public void guardar() {
        try{
            //SE OBTIENE LA CANTIDAD DE REFERENCIAS SEGUN EL ID DE LA REFERENCIA
            referencia = referenciaEJB.CantidadReferencias(idreferencia);
            int ReferenciasStock = referencia.getRefCantidadStock();
            
            if(ReferenciasStock<cantidad){
                this.mensaje = "SOLO HAY "+ReferenciasStock+" REFERENCIAS EN EL INVENTARIO";
            }else{
                usuario = usuarioEJB.findUusario(idusuario);
                idlistaprecio = usuario.getUsuListaPrecio().getLispreId();
                int user = usuario.getUsuId();
                        
                //LISTAMOS LOS REGISTROS DE LA TABLA ReferenciaListaPrecio 
                List<ReferenciaListaPrecio> lista_reflispre = reflispreEJB.findAll();
                
                for(ReferenciaListaPrecio r : lista_reflispre){
                    //SI EL ID DE LA LISTA DE PRECIO Y DE LA REFERENCIA SELECCIONADA EN EL FORMULARIO DE VENTAS, SON IGUALES A LOS DE LA TABLA ReferenciaListaPrecio
                    if(r.getReferencia().getRefId()==idreferencia && r.getListaPrecio().getLispreId() == idlistaprecio){
                        //ALMACENE EL VALOR DE LA REFERENCIA, SEGUN LISTA DE PRECIO Y REFERENCIA EN LA VARIABLE precio
                        precio = r.getReflispreValor();
                        descuento = r.getReflispreDescuento();
                    }
                }
                
                
                double iva =((cantidad*precio)*0.19);
                venta.setVenIva(iva);
                venta.setVenCantidad(cantidad);
                venta.setVenPrecio(precio);
                venta.setVenSubtotal((cantidad*precio)+iva);
                venta.setVenDescuento(descuento);
                venta.setVenTotal((cantidad*precio)+(iva-descuento));

                venta.setVenUsuario(new Usuario(user));
                venta.setVenReferencia(new Referencia(idreferencia));
                venta.setVenSede(new Sede(idsede));

                ventaEJB.create(venta);
                venta = new Venta();
                this.mensaje = "Registrado Correctamente";
            }
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void cargaId (Venta venta){
        venta = new Venta();
    }
    
    public void eliminar(Venta u){
        try{
            ventaEJB.remove(u);
            venta = new Venta();
            venta.setVenUsuario(new Usuario());
            venta.setVenReferencia(new Referencia());
            mensaje="Se elimino la Venta Satisfactoriamente!";
        }catch (Exception e){
            mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null,msj);
    }
    
    public void limpiar(){
        venta = new Venta();
        venta.setVenUsuario(new Usuario());
        venta.setVenReferencia(new Referencia());
    }    
}