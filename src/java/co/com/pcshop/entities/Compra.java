/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "compra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compra.findAll", query = "SELECT c FROM Compra c")
    , @NamedQuery(name = "Compra.findByComId", query = "SELECT c FROM Compra c WHERE c.comId = :comId")
    , @NamedQuery(name = "Compra.findByComFechaCompra", query = "SELECT c FROM Compra c WHERE c.comFechaCompra = :comFechaCompra")
    , @NamedQuery(name = "Compra.findByComSubtotal", query = "SELECT c FROM Compra c WHERE c.comSubtotal = :comSubtotal")
    , @NamedQuery(name = "Compra.findByComIva", query = "SELECT c FROM Compra c WHERE c.comIva = :comIva")
    , @NamedQuery(name = "Compra.findByComDescuento", query = "SELECT c FROM Compra c WHERE c.comDescuento = :comDescuento")
    , @NamedQuery(name = "Compra.findByComTotal", query = "SELECT c FROM Compra c WHERE c.comTotal = :comTotal")
    , @NamedQuery(name = "Compra.findByComCantidad", query = "SELECT c FROM Compra c WHERE c.comCantidad = :comCantidad")
    , @NamedQuery(name = "Compra.findByComPrecio", query = "SELECT c FROM Compra c WHERE c.comPrecio = :comPrecio")})
public class Compra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "com_id")
    private Integer comId;
    @Column(name = "com_fecha_compra", insertable = false, updatable = false)
    @Temporal(TemporalType.DATE)
    private Date comFechaCompra;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "com_subtotal")
    private Double comSubtotal;
    @Column(name = "com_iva")
    private Double comIva;
    @Column(name = "com_descuento")
    private Double comDescuento;
    @Column(name = "com_total")
    private Double comTotal;
    @Column(name = "com_cantidad")
    private Double comCantidad;
    @Column(name = "com_precio")
    private Double comPrecio;
    @JoinColumn(name = "com_proveedor", referencedColumnName = "pro_id")
    @ManyToOne
    private Proveedor comProveedor;
    @JoinColumn(name = "com_usuario", referencedColumnName = "usu_id")
    @ManyToOne
    private Usuario comUsuario;
    @JoinColumn(name = "com_referencia", referencedColumnName = "ref_id")
    @ManyToOne
    private Referencia comReferencia;
    @JoinColumn(name = "com_sede", referencedColumnName = "sed_id")
    @ManyToOne(optional = false)
    private Sede comSede;

    public Compra() {
    }

    public Compra(Integer comId) {
        this.comId = comId;
    }

    public Integer getComId() {
        return comId;
    }

    public void setComId(Integer comId) {
        this.comId = comId;
    }

    public Date getComFechaCompra() {
        return comFechaCompra;
    }

    public void setComFechaCompra(Date comFechaCompra) {
        this.comFechaCompra = comFechaCompra;
    }

    public Double getComSubtotal() {
        return comSubtotal;
    }

    public void setComSubtotal(Double comSubtotal) {
        this.comSubtotal = comSubtotal;
    }

    public Double getComIva() {
        return comIva;
    }

    public void setComIva(Double comIva) {
        this.comIva = comIva;
    }

    public Double getComDescuento() {
        return comDescuento;
    }

    public void setComDescuento(Double comDescuento) {
        this.comDescuento = comDescuento;
    }

    public Double getComTotal() {
        return comTotal;
    }

    public void setComTotal(Double comTotal) {
        this.comTotal = comTotal;
    }

    public Double getComCantidad() {
        return comCantidad;
    }

    public void setComCantidad(Double comCantidad) {
        this.comCantidad = comCantidad;
    }

    public Double getComPrecio() {
        return comPrecio;
    }

    public void setComPrecio(Double comPrecio) {
        this.comPrecio = comPrecio;
    }

    public Proveedor getComProveedor() {
        return comProveedor;
    }

    public void setComProveedor(Proveedor comProveedor) {
        this.comProveedor = comProveedor;
    }

    public Usuario getComUsuario() {
        return comUsuario;
    }

    public void setComUsuario(Usuario comUsuario) {
        this.comUsuario = comUsuario;
    }

    public Referencia getComReferencia() {
        return comReferencia;
    }

    public void setComReferencia(Referencia comReferencia) {
        this.comReferencia = comReferencia;
    }

    public Sede getComSede() {
        return comSede;
    }

    public void setComSede(Sede comSede) {
        this.comSede = comSede;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comId != null ? comId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compra)) {
            return false;
        }
        Compra other = (Compra) object;
        if ((this.comId == null && other.comId != null) || (this.comId != null && !this.comId.equals(other.comId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Compra[ comId=" + comId + " ]";
    }
    
}
