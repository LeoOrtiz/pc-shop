/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "descuentoref_lispre")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DescuentorefLispre.findAll", query = "SELECT d FROM DescuentorefLispre d")
    , @NamedQuery(name = "DescuentorefLispre.findByRefsedReferencia", query = "SELECT d FROM DescuentorefLispre d WHERE d.refsedReferencia = :refsedReferencia")
    , @NamedQuery(name = "DescuentorefLispre.findByRefsedSede", query = "SELECT d FROM DescuentorefLispre d WHERE d.refsedSede = :refsedSede")
    , @NamedQuery(name = "DescuentorefLispre.findByRefsedCantidad", query = "SELECT d FROM DescuentorefLispre d WHERE d.refsedCantidad = :refsedCantidad")
    , @NamedQuery(name = "DescuentorefLispre.findByReflispreReferencia", query = "SELECT d FROM DescuentorefLispre d WHERE d.reflispreReferencia = :reflispreReferencia")
    , @NamedQuery(name = "DescuentorefLispre.findByReflispreListaPrecio", query = "SELECT d FROM DescuentorefLispre d WHERE d.reflispreListaPrecio = :reflispreListaPrecio")
    , @NamedQuery(name = "DescuentorefLispre.findByReflispreValor", query = "SELECT d FROM DescuentorefLispre d WHERE d.reflispreValor = :reflispreValor")
    , @NamedQuery(name = "DescuentorefLispre.findByReflispreDescuento", query = "SELECT d FROM DescuentorefLispre d WHERE d.reflispreDescuento = :reflispreDescuento")
    , @NamedQuery(name = "DescuentorefLispre.findByRefId", query = "SELECT d FROM DescuentorefLispre d WHERE d.refId = :refId")
    , @NamedQuery(name = "DescuentorefLispre.findByRefFoto", query = "SELECT d FROM DescuentorefLispre d WHERE d.refFoto = :refFoto")
    , @NamedQuery(name = "DescuentorefLispre.findByRefNombre", query = "SELECT d FROM DescuentorefLispre d WHERE d.refNombre = :refNombre")
    , @NamedQuery(name = "DescuentorefLispre.findByRefNota", query = "SELECT d FROM DescuentorefLispre d WHERE d.refNota = :refNota")
    , @NamedQuery(name = "DescuentorefLispre.findByRefLinea", query = "SELECT d FROM DescuentorefLispre d WHERE d.refLinea = :refLinea")
    , @NamedQuery(name = "DescuentorefLispre.findByRefMarca", query = "SELECT d FROM DescuentorefLispre d WHERE d.refMarca = :refMarca")
    , @NamedQuery(name = "DescuentorefLispre.findByRefCantidadStock", query = "SELECT d FROM DescuentorefLispre d WHERE d.refCantidadStock = :refCantidadStock")
    , @NamedQuery(name = "DescuentorefLispre.findByMarId", query = "SELECT d FROM DescuentorefLispre d WHERE d.marId = :marId")
    , @NamedQuery(name = "DescuentorefLispre.findByMarFoto", query = "SELECT d FROM DescuentorefLispre d WHERE d.marFoto = :marFoto")
    , @NamedQuery(name = "DescuentorefLispre.findByMarNombre", query = "SELECT d FROM DescuentorefLispre d WHERE d.marNombre = :marNombre")
    , @NamedQuery(name = "DescuentorefLispre.findBySedId", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedId = :sedId")
    , @NamedQuery(name = "DescuentorefLispre.findBySedNombre", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedNombre = :sedNombre")
    , @NamedQuery(name = "DescuentorefLispre.findBySedEmpresa", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedEmpresa = :sedEmpresa")
    , @NamedQuery(name = "DescuentorefLispre.findBySedUbicacion", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedUbicacion = :sedUbicacion")
    , @NamedQuery(name = "DescuentorefLispre.findBySedFoto", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedFoto = :sedFoto")
    , @NamedQuery(name = "DescuentorefLispre.findBySedDireccion", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedDireccion = :sedDireccion")
    , @NamedQuery(name = "DescuentorefLispre.findBySedTelefono", query = "SELECT d FROM DescuentorefLispre d WHERE d.sedTelefono = :sedTelefono")
    , @NamedQuery(name = "DescuentorefLispre.findByLispreId", query = "SELECT d FROM DescuentorefLispre d WHERE d.lispreId = :lispreId")
    , @NamedQuery(name = "DescuentorefLispre.findByLispreNombre", query = "SELECT d FROM DescuentorefLispre d WHERE d.lispreNombre = :lispreNombre")
    , @NamedQuery(name = "DescuentorefLispre.findByUbiId", query = "SELECT d FROM DescuentorefLispre d WHERE d.ubiId = :ubiId")
    , @NamedQuery(name = "DescuentorefLispre.findByUbiLatitud", query = "SELECT d FROM DescuentorefLispre d WHERE d.ubiLatitud = :ubiLatitud")
    , @NamedQuery(name = "DescuentorefLispre.findByUbiLongitud", query = "SELECT d FROM DescuentorefLispre d WHERE d.ubiLongitud = :ubiLongitud")})
public class DescuentorefLispre implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_referencia")
    private int refsedReferencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_sede")
    private int refsedSede;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_cantidad")
    private int refsedCantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_referencia")
    private int reflispreReferencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_lista_precio")
    private int reflispreListaPrecio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_valor")
    private double reflispreValor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_descuento")
    private double reflispreDescuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ref_id")
    @Id
    private int refId;
    @Size(max = 100)
    @Column(name = "ref_foto")
    private String refFoto;
    @Size(max = 40)
    @Column(name = "ref_nombre")
    private String refNombre;
    @Size(max = 255)
    @Column(name = "ref_nota")
    private String refNota;
    @Column(name = "ref_linea")
    private Integer refLinea;
    @Column(name = "ref_marca")
    private Integer refMarca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ref_cantidad_stock")
    private int refCantidadStock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mar_id")
    @Id
    private int marId;
    @Size(max = 255)
    @Column(name = "mar_foto")
    private String marFoto;
    @Size(max = 40)
    @Column(name = "mar_nombre")
    private String marNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sed_id")
    @Id
    private int sedId;
    @Size(max = 40)
    @Column(name = "sed_nombre")
    private String sedNombre;
    @Column(name = "sed_empresa")
    private Integer sedEmpresa;
    @Column(name = "sed_ubicacion")
    private Integer sedUbicacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sed_foto")
    private String sedFoto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "sed_direccion")
    private String sedDireccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "sed_telefono")
    private String sedTelefono;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lispre_id")
    @Id
    private int lispreId;
    @Size(max = 40)
    @Column(name = "lispre_nombre")
    private String lispreNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ubi_id")
    @Id
    private int ubiId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ubi_latitud")
    private Double ubiLatitud;
    @Column(name = "ubi_longitud")
    private Double ubiLongitud;

    public DescuentorefLispre() {
    }

    public int getRefsedReferencia() {
        return refsedReferencia;
    }

    public void setRefsedReferencia(int refsedReferencia) {
        this.refsedReferencia = refsedReferencia;
    }

    public int getRefsedSede() {
        return refsedSede;
    }

    public void setRefsedSede(int refsedSede) {
        this.refsedSede = refsedSede;
    }

    public int getRefsedCantidad() {
        return refsedCantidad;
    }

    public void setRefsedCantidad(int refsedCantidad) {
        this.refsedCantidad = refsedCantidad;
    }

    public int getReflispreReferencia() {
        return reflispreReferencia;
    }

    public void setReflispreReferencia(int reflispreReferencia) {
        this.reflispreReferencia = reflispreReferencia;
    }

    public int getReflispreListaPrecio() {
        return reflispreListaPrecio;
    }

    public void setReflispreListaPrecio(int reflispreListaPrecio) {
        this.reflispreListaPrecio = reflispreListaPrecio;
    }

    public double getReflispreValor() {
        return reflispreValor;
    }

    public void setReflispreValor(double reflispreValor) {
        this.reflispreValor = reflispreValor;
    }

    public double getReflispreDescuento() {
        return reflispreDescuento;
    }

    public void setReflispreDescuento(double reflispreDescuento) {
        this.reflispreDescuento = reflispreDescuento;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }

    public String getRefFoto() {
        return refFoto;
    }

    public void setRefFoto(String refFoto) {
        this.refFoto = refFoto;
    }

    public String getRefNombre() {
        return refNombre;
    }

    public void setRefNombre(String refNombre) {
        this.refNombre = refNombre;
    }

    public String getRefNota() {
        return refNota;
    }

    public void setRefNota(String refNota) {
        this.refNota = refNota;
    }

    public Integer getRefLinea() {
        return refLinea;
    }

    public void setRefLinea(Integer refLinea) {
        this.refLinea = refLinea;
    }

    public Integer getRefMarca() {
        return refMarca;
    }

    public void setRefMarca(Integer refMarca) {
        this.refMarca = refMarca;
    }

    public int getRefCantidadStock() {
        return refCantidadStock;
    }

    public void setRefCantidadStock(int refCantidadStock) {
        this.refCantidadStock = refCantidadStock;
    }

    public int getMarId() {
        return marId;
    }

    public void setMarId(int marId) {
        this.marId = marId;
    }

    public String getMarFoto() {
        return marFoto;
    }

    public void setMarFoto(String marFoto) {
        this.marFoto = marFoto;
    }

    public String getMarNombre() {
        return marNombre;
    }

    public void setMarNombre(String marNombre) {
        this.marNombre = marNombre;
    }

    public int getSedId() {
        return sedId;
    }

    public void setSedId(int sedId) {
        this.sedId = sedId;
    }

    public String getSedNombre() {
        return sedNombre;
    }

    public void setSedNombre(String sedNombre) {
        this.sedNombre = sedNombre;
    }

    public Integer getSedEmpresa() {
        return sedEmpresa;
    }

    public void setSedEmpresa(Integer sedEmpresa) {
        this.sedEmpresa = sedEmpresa;
    }

    public Integer getSedUbicacion() {
        return sedUbicacion;
    }

    public void setSedUbicacion(Integer sedUbicacion) {
        this.sedUbicacion = sedUbicacion;
    }

    public String getSedFoto() {
        return sedFoto;
    }

    public void setSedFoto(String sedFoto) {
        this.sedFoto = sedFoto;
    }

    public String getSedDireccion() {
        return sedDireccion;
    }

    public void setSedDireccion(String sedDireccion) {
        this.sedDireccion = sedDireccion;
    }

    public String getSedTelefono() {
        return sedTelefono;
    }

    public void setSedTelefono(String sedTelefono) {
        this.sedTelefono = sedTelefono;
    }

    public int getLispreId() {
        return lispreId;
    }

    public void setLispreId(int lispreId) {
        this.lispreId = lispreId;
    }

    public String getLispreNombre() {
        return lispreNombre;
    }

    public void setLispreNombre(String lispreNombre) {
        this.lispreNombre = lispreNombre;
    }

    public int getUbiId() {
        return ubiId;
    }

    public void setUbiId(int ubiId) {
        this.ubiId = ubiId;
    }

    public Double getUbiLatitud() {
        return ubiLatitud;
    }

    public void setUbiLatitud(Double ubiLatitud) {
        this.ubiLatitud = ubiLatitud;
    }

    public Double getUbiLongitud() {
        return ubiLongitud;
    }

    public void setUbiLongitud(Double ubiLongitud) {
        this.ubiLongitud = ubiLongitud;
    }
    
}
