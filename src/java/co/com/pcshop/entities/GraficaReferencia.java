/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "grafica_referencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GraficaReferencia.findAll", query = "SELECT g FROM GraficaReferencia g")
    , @NamedQuery(name = "GraficaReferencia.findByRefsedReferencia", query = "SELECT g FROM GraficaReferencia g WHERE g.refsedReferencia = :refsedReferencia")
    , @NamedQuery(name = "GraficaReferencia.findByRefsedSede", query = "SELECT g FROM GraficaReferencia g WHERE g.refsedSede = :refsedSede")
    , @NamedQuery(name = "GraficaReferencia.findByRefsedCantidad", query = "SELECT g FROM GraficaReferencia g WHERE g.refsedCantidad = :refsedCantidad")
    , @NamedQuery(name = "GraficaReferencia.findByRefId", query = "SELECT g FROM GraficaReferencia g WHERE g.refId = :refId")
    , @NamedQuery(name = "GraficaReferencia.findByRefFoto", query = "SELECT g FROM GraficaReferencia g WHERE g.refFoto = :refFoto")
    , @NamedQuery(name = "GraficaReferencia.findByRefNombre", query = "SELECT g FROM GraficaReferencia g WHERE g.refNombre = :refNombre")
    , @NamedQuery(name = "GraficaReferencia.findByRefNota", query = "SELECT g FROM GraficaReferencia g WHERE g.refNota = :refNota")
    , @NamedQuery(name = "GraficaReferencia.findByRefLinea", query = "SELECT g FROM GraficaReferencia g WHERE g.refLinea = :refLinea")
    , @NamedQuery(name = "GraficaReferencia.findByRefMarca", query = "SELECT g FROM GraficaReferencia g WHERE g.refMarca = :refMarca")
    , @NamedQuery(name = "GraficaReferencia.findByRefCantidadStock", query = "SELECT g FROM GraficaReferencia g WHERE g.refCantidadStock = :refCantidadStock")
    , @NamedQuery(name = "GraficaReferencia.findBySedId", query = "SELECT g FROM GraficaReferencia g WHERE g.sedId = :sedId")
    , @NamedQuery(name = "GraficaReferencia.findBySedNombre", query = "SELECT g FROM GraficaReferencia g WHERE g.sedNombre = :sedNombre")
    , @NamedQuery(name = "GraficaReferencia.findBySedEmpresa", query = "SELECT g FROM GraficaReferencia g WHERE g.sedEmpresa = :sedEmpresa")
    , @NamedQuery(name = "GraficaReferencia.findBySedUbicacion", query = "SELECT g FROM GraficaReferencia g WHERE g.sedUbicacion = :sedUbicacion")
    , @NamedQuery(name = "GraficaReferencia.findBySedFoto", query = "SELECT g FROM GraficaReferencia g WHERE g.sedFoto = :sedFoto")
    , @NamedQuery(name = "GraficaReferencia.findBySedDireccion", query = "SELECT g FROM GraficaReferencia g WHERE g.sedDireccion = :sedDireccion")
    , @NamedQuery(name = "GraficaReferencia.findBySedTelefono", query = "SELECT g FROM GraficaReferencia g WHERE g.sedTelefono = :sedTelefono")})
public class GraficaReferencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_referencia")
    private int refsedReferencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_sede")
    private int refsedSede;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_cantidad")
    private int refsedCantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ref_id")
    @Id
    private int refId;
    @Size(max = 100)
    @Column(name = "ref_foto")
    private String refFoto;
    @Size(max = 40)
    @Column(name = "ref_nombre")
    private String refNombre;
    @Size(max = 255)
    @Column(name = "ref_nota")
    private String refNota;
    @Column(name = "ref_linea")
    private Integer refLinea;
    @Column(name = "ref_marca")
    private Integer refMarca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ref_cantidad_stock")
    private int refCantidadStock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sed_id")
    @Id
    private int sedId;
    @Size(max = 40)
    @Column(name = "sed_nombre")
    private String sedNombre;
    @Column(name = "sed_empresa")
    private Integer sedEmpresa;
    @Column(name = "sed_ubicacion")
    private Integer sedUbicacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sed_foto")
    private String sedFoto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "sed_direccion")
    private String sedDireccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "sed_telefono")
    private String sedTelefono;

    public GraficaReferencia() {
    }

    public int getRefsedReferencia() {
        return refsedReferencia;
    }

    public void setRefsedReferencia(int refsedReferencia) {
        this.refsedReferencia = refsedReferencia;
    }

    public int getRefsedSede() {
        return refsedSede;
    }

    public void setRefsedSede(int refsedSede) {
        this.refsedSede = refsedSede;
    }

    public int getRefsedCantidad() {
        return refsedCantidad;
    }

    public void setRefsedCantidad(int refsedCantidad) {
        this.refsedCantidad = refsedCantidad;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }

    public String getRefFoto() {
        return refFoto;
    }

    public void setRefFoto(String refFoto) {
        this.refFoto = refFoto;
    }

    public String getRefNombre() {
        return refNombre;
    }

    public void setRefNombre(String refNombre) {
        this.refNombre = refNombre;
    }

    public String getRefNota() {
        return refNota;
    }

    public void setRefNota(String refNota) {
        this.refNota = refNota;
    }

    public Integer getRefLinea() {
        return refLinea;
    }

    public void setRefLinea(Integer refLinea) {
        this.refLinea = refLinea;
    }

    public Integer getRefMarca() {
        return refMarca;
    }

    public void setRefMarca(Integer refMarca) {
        this.refMarca = refMarca;
    }

    public int getRefCantidadStock() {
        return refCantidadStock;
    }

    public void setRefCantidadStock(int refCantidadStock) {
        this.refCantidadStock = refCantidadStock;
    }

    public int getSedId() {
        return sedId;
    }

    public void setSedId(int sedId) {
        this.sedId = sedId;
    }

    public String getSedNombre() {
        return sedNombre;
    }

    public void setSedNombre(String sedNombre) {
        this.sedNombre = sedNombre;
    }

    public Integer getSedEmpresa() {
        return sedEmpresa;
    }

    public void setSedEmpresa(Integer sedEmpresa) {
        this.sedEmpresa = sedEmpresa;
    }

    public Integer getSedUbicacion() {
        return sedUbicacion;
    }

    public void setSedUbicacion(Integer sedUbicacion) {
        this.sedUbicacion = sedUbicacion;
    }

    public String getSedFoto() {
        return sedFoto;
    }

    public void setSedFoto(String sedFoto) {
        this.sedFoto = sedFoto;
    }

    public String getSedDireccion() {
        return sedDireccion;
    }

    public void setSedDireccion(String sedDireccion) {
        this.sedDireccion = sedDireccion;
    }

    public String getSedTelefono() {
        return sedTelefono;
    }

    public void setSedTelefono(String sedTelefono) {
        this.sedTelefono = sedTelefono;
    }
    
}
