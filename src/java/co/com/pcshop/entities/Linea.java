/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "linea")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Linea.findAll", query = "SELECT l FROM Linea l")
    , @NamedQuery(name = "Linea.findByLinId", query = "SELECT l FROM Linea l WHERE l.linId = :linId")
    , @NamedQuery(name = "Linea.findByLinNombre", query = "SELECT l FROM Linea l WHERE l.linNombre = :linNombre")})
public class Linea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "lin_id")
    private Integer linId;
    @Size(max = 100)
    @Column(name = "lin_nombre")
    private String linNombre;
    @OneToMany(mappedBy = "refLinea")
    private Collection<Referencia> referenciaCollection;

    public Linea() {
    }

    public Linea(Integer linId) {
        this.linId = linId;
    }

    public Integer getLinId() {
        return linId;
    }

    public void setLinId(Integer linId) {
        this.linId = linId;
    }

    public String getLinNombre() {
        return linNombre;
    }

    public void setLinNombre(String linNombre) {
        this.linNombre = linNombre;
    }

    @XmlTransient
    public Collection<Referencia> getReferenciaCollection() {
        return referenciaCollection;
    }

    public void setReferenciaCollection(Collection<Referencia> referenciaCollection) {
        this.referenciaCollection = referenciaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linId != null ? linId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Linea)) {
            return false;
        }
        Linea other = (Linea) object;
        if ((this.linId == null && other.linId != null) || (this.linId != null && !this.linId.equals(other.linId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Linea[ linId=" + linId + " ]";
    }
    
}
