/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "lista_precio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListaPrecio.findAll", query = "SELECT l FROM ListaPrecio l")
    , @NamedQuery(name = "ListaPrecio.findByLispreId", query = "SELECT l FROM ListaPrecio l WHERE l.lispreId = :lispreId")
    , @NamedQuery(name = "ListaPrecio.findByLispreNombre", query = "SELECT l FROM ListaPrecio l WHERE l.lispreNombre = :lispreNombre")})
public class ListaPrecio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "lispre_id")
    private Integer lispreId;
    @Size(max = 40)
    @Column(name = "lispre_nombre")
    private String lispreNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "listaPrecio")
    private Collection<ReferenciaListaPrecio> referenciaListaPrecioCollection;

    public ListaPrecio() {
    }

    public ListaPrecio(Integer lispreId) {
        this.lispreId = lispreId;
    }

    public Integer getLispreId() {
        return lispreId;
    }

    public void setLispreId(Integer lispreId) {
        this.lispreId = lispreId;
    }

    public String getLispreNombre() {
        return lispreNombre;
    }

    public void setLispreNombre(String lispreNombre) {
        this.lispreNombre = lispreNombre;
    }

    @XmlTransient
    public Collection<ReferenciaListaPrecio> getReferenciaListaPrecioCollection() {
        return referenciaListaPrecioCollection;
    }

    public void setReferenciaListaPrecioCollection(Collection<ReferenciaListaPrecio> referenciaListaPrecioCollection) {
        this.referenciaListaPrecioCollection = referenciaListaPrecioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lispreId != null ? lispreId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaPrecio)) {
            return false;
        }
        ListaPrecio other = (ListaPrecio) object;
        if ((this.lispreId == null && other.lispreId != null) || (this.lispreId != null && !this.lispreId.equals(other.lispreId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.ListaPrecio[ lispreId=" + lispreId + " ]";
    }
    
}
