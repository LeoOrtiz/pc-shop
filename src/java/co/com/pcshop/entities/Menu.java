/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m")
    , @NamedQuery(name = "Menu.findByMenCodigo", query = "SELECT m FROM Menu m WHERE m.menCodigo = :menCodigo")
    , @NamedQuery(name = "Menu.findByMenNombre", query = "SELECT m FROM Menu m WHERE m.menNombre = :menNombre")
    , @NamedQuery(name = "Menu.findByMenuTipo", query = "SELECT m FROM Menu m WHERE m.menuTipo = :menuTipo")
    , @NamedQuery(name = "Menu.findByMenEstado", query = "SELECT m FROM Menu m WHERE m.menEstado = :menEstado")
    , @NamedQuery(name = "Menu.findByMenUrl", query = "SELECT m FROM Menu m WHERE m.menUrl = :menUrl")
    , @NamedQuery(name = "Menu.findByMenGeneral", query = "SELECT m FROM Menu m WHERE m.menuTipo = 'S'")
    , @NamedQuery(name = "Menu.findByMenUltimoId", query = "SELECT m FROM Menu m ORDER BY m.menCodigo DESC")})

public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "men_codigo")
    private Integer menCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "men_nombre")
    private String menNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "menu_tipo")
    private String menuTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "men_estado")
    private boolean menEstado;
    @Size(max = 100)
    @Column(name = "men_url")
    private String menUrl;
    @OneToMany(mappedBy = "menCodigoSubmenu")
    private Collection<Menu> menuCollection;
    @JoinColumn(name = "men_codigo_submenu", referencedColumnName = "men_codigo")
    @ManyToOne
    private Menu menCodigoSubmenu;
    @JoinColumn(name = "menu_tipo_usuario", referencedColumnName = "tipusu_id")
    @ManyToOne
    private TipoUsuario menuTipoUsuario;

    public Menu() {
    }

    public Menu(Integer menCodigo) {
        this.menCodigo = menCodigo;
    }

    public Menu(Integer menCodigo, String menNombre, String menuTipo, boolean menEstado) {
        this.menCodigo = menCodigo;
        this.menNombre = menNombre;
        this.menuTipo = menuTipo;
        this.menEstado = menEstado;
    }

    public Integer getMenCodigo() {
        return menCodigo;
    }

    public void setMenCodigo(Integer menCodigo) {
        this.menCodigo = menCodigo;
    }

    public String getMenNombre() {
        return menNombre;
    }

    public void setMenNombre(String menNombre) {
        this.menNombre = menNombre;
    }

    public String getMenuTipo() {
        return menuTipo;
    }

    public void setMenuTipo(String menuTipo) {
        this.menuTipo = menuTipo;
    }

    public boolean getMenEstado() {
        return menEstado;
    }

    public void setMenEstado(boolean menEstado) {
        this.menEstado = menEstado;
    }

    public String getMenUrl() {
        return menUrl;
    }

    public void setMenUrl(String menUrl) {
        this.menUrl = menUrl;
    }

    @XmlTransient
    public Collection<Menu> getMenuCollection() {
        return menuCollection;
    }

    public void setMenuCollection(Collection<Menu> menuCollection) {
        this.menuCollection = menuCollection;
    }

    public Menu getMenCodigoSubmenu() {
        return menCodigoSubmenu;
    }

    public void setMenCodigoSubmenu(Menu menCodigoSubmenu) {
        this.menCodigoSubmenu = menCodigoSubmenu;
    }

    public TipoUsuario getMenuTipoUsuario() {
        return menuTipoUsuario;
    }

    public void setMenuTipoUsuario(TipoUsuario menuTipoUsuario) {
        this.menuTipoUsuario = menuTipoUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menCodigo != null ? menCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.menCodigo == null && other.menCodigo != null) || (this.menCodigo != null && !this.menCodigo.equals(other.menCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Menu[ menCodigo=" + menCodigo + " ]";
    }
    
}
