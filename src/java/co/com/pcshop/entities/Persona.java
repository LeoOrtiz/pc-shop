/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "persona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")
    , @NamedQuery(name = "Persona.findByPerId", query = "SELECT p FROM Persona p WHERE p.perId = :perId")
    , @NamedQuery(name = "Persona.findByPerEmail", query = "SELECT p FROM Persona p WHERE p.perEmail = :perEmail")
    , @NamedQuery(name = "Persona.findByPerFechaCreacion", query = "SELECT p FROM Persona p WHERE p.perFechaCreacion = :perFechaCreacion")
    , @NamedQuery(name = "Persona.findByPerPrimerApellido", query = "SELECT p FROM Persona p WHERE p.perPrimerApellido = :perPrimerApellido")
    , @NamedQuery(name = "Persona.findByPerPrimerNombre", query = "SELECT p FROM Persona p WHERE p.perPrimerNombre = :perPrimerNombre")
    , @NamedQuery(name = "Persona.findByPerSegundoApellido", query = "SELECT p FROM Persona p WHERE p.perSegundoApellido = :perSegundoApellido")
    , @NamedQuery(name = "Persona.findByPerSegundoNombre", query = "SELECT p FROM Persona p WHERE p.perSegundoNombre = :perSegundoNombre")
    , @NamedQuery(name = "Persona.findByPerNumeroDocumento", query = "SELECT p FROM Persona p WHERE p.perNumeroDocumento = :perNumeroDocumento")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "per_id")
    private Integer perId;
    @Size(max = 100)
    @Column(name = "per_email")
    private String perEmail;
    @Column(name = "per_fecha_creacion")
    @Temporal(TemporalType.DATE)
    private Date perFechaCreacion;
    @Size(max = 20)
    @Column(name = "per_primer_apellido")
    private String perPrimerApellido;
    @Size(max = 20)
    @Column(name = "per_primer_nombre")
    private String perPrimerNombre;
    @Size(max = 20)
    @Column(name = "per_segundo_apellido")
    private String perSegundoApellido;
    @Size(max = 20)
    @Column(name = "per_segundo_nombre")
    private String perSegundoNombre;
    @Size(max = 20)
    @Column(name = "per_numero_documento")
    private String perNumeroDocumento;
    @JoinColumn(name = "per_tipo_documento", referencedColumnName = "tipdoc_id")
    @ManyToOne
    private TipoDocumento perTipoDocumento;
    @OneToMany(mappedBy = "usuPersona")
    private Collection<Usuario> usuarioCollection;

    public Persona() {
    }

    public Persona(Integer perId) {
        this.perId = perId;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getPerEmail() {
        return perEmail;
    }

    public void setPerEmail(String perEmail) {
        this.perEmail = perEmail;
    }

    public Date getPerFechaCreacion() {
        return perFechaCreacion;
    }

    public void setPerFechaCreacion(Date perFechaCreacion) {
        this.perFechaCreacion = perFechaCreacion;
    }

    public String getPerPrimerApellido() {
        return perPrimerApellido;
    }

    public void setPerPrimerApellido(String perPrimerApellido) {
        this.perPrimerApellido = perPrimerApellido;
    }

    public String getPerPrimerNombre() {
        return perPrimerNombre;
    }

    public void setPerPrimerNombre(String perPrimerNombre) {
        this.perPrimerNombre = perPrimerNombre;
    }

    public String getPerSegundoApellido() {
        return perSegundoApellido;
    }

    public void setPerSegundoApellido(String perSegundoApellido) {
        this.perSegundoApellido = perSegundoApellido;
    }

    public String getPerSegundoNombre() {
        return perSegundoNombre;
    }

    public void setPerSegundoNombre(String perSegundoNombre) {
        this.perSegundoNombre = perSegundoNombre;
    }

    public String getPerNumeroDocumento() {
        return perNumeroDocumento;
    }

    public void setPerNumeroDocumento(String perNumeroDocumento) {
        this.perNumeroDocumento = perNumeroDocumento;
    }

    public TipoDocumento getPerTipoDocumento() {
        return perTipoDocumento;
    }

    public void setPerTipoDocumento(TipoDocumento perTipoDocumento) {
        this.perTipoDocumento = perTipoDocumento;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perId != null ? perId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.perId == null && other.perId != null) || (this.perId != null && !this.perId.equals(other.perId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Persona[ perId=" + perId + " ]";
    }
    
}
