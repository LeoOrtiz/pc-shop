/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "proveedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedor.findAll", query = "SELECT p FROM Proveedor p")
    , @NamedQuery(name = "Proveedor.findByProId", query = "SELECT p FROM Proveedor p WHERE p.proId = :proId")
    , @NamedQuery(name = "Proveedor.findByProCompa\u00f1ia", query = "SELECT p FROM Proveedor p WHERE p.proCompa\u00f1ia = :proCompa\u00f1ia")
    , @NamedQuery(name = "Proveedor.findByProContacto", query = "SELECT p FROM Proveedor p WHERE p.proContacto = :proContacto")
    , @NamedQuery(name = "Proveedor.findByProNumeroDocumento", query = "SELECT p FROM Proveedor p WHERE p.proNumeroDocumento = :proNumeroDocumento")})
public class Proveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pro_id")
    private Integer proId;
    @Size(max = 45)
    @Column(name = "pro_compa\u00f1ia")
    private String proCompañia;
    @Size(max = 45)
    @Column(name = "pro_contacto")
    private String proContacto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "pro_numero_documento")
    private String proNumeroDocumento;
    @OneToMany(mappedBy = "comProveedor")
    private Collection<Compra> compraCollection;
    @JoinColumn(name = "pro_estado", referencedColumnName = "est_id")
    @ManyToOne
    private Estado proEstado;
    @JoinColumn(name = "pro_tipo_documento", referencedColumnName = "tipdoc_id")
    @ManyToOne
    private TipoDocumento proTipoDocumento;

    public Proveedor() {
    }

    public Proveedor(Integer proId) {
        this.proId = proId;
    }

    public Proveedor(Integer proId, String proNumeroDocumento) {
        this.proId = proId;
        this.proNumeroDocumento = proNumeroDocumento;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getProCompañia() {
        return proCompañia;
    }

    public void setProCompañia(String proCompañia) {
        this.proCompañia = proCompañia;
    }

    public String getProContacto() {
        return proContacto;
    }

    public void setProContacto(String proContacto) {
        this.proContacto = proContacto;
    }

    public String getProNumeroDocumento() {
        return proNumeroDocumento;
    }

    public void setProNumeroDocumento(String proNumeroDocumento) {
        this.proNumeroDocumento = proNumeroDocumento;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    public Estado getProEstado() {
        return proEstado;
    }

    public void setProEstado(Estado proEstado) {
        this.proEstado = proEstado;
    }

    public TipoDocumento getProTipoDocumento() {
        return proTipoDocumento;
    }

    public void setProTipoDocumento(TipoDocumento proTipoDocumento) {
        this.proTipoDocumento = proTipoDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proId != null ? proId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedor)) {
            return false;
        }
        Proveedor other = (Proveedor) object;
        if ((this.proId == null && other.proId != null) || (this.proId != null && !this.proId.equals(other.proId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Proveedor[ proId=" + proId + " ]";
    }
    
}
