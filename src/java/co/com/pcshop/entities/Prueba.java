/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "prueba")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prueba.findAll", query = "SELECT p FROM Prueba p")
    , @NamedQuery(name = "Prueba.findByPId", query = "SELECT p FROM Prueba p WHERE p.pId = :pId")
    , @NamedQuery(name = "Prueba.findByPReferencia", query = "SELECT p FROM Prueba p WHERE p.pReferencia = :pReferencia")
    , @NamedQuery(name = "Prueba.findByPSede", query = "SELECT p FROM Prueba p WHERE p.pSede = :pSede")})
public class Prueba implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "p_id")
    private Integer pId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "p_referencia")
    private int pReferencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "p_sede")
    private int pSede;

    public Prueba() {
    }

    public Prueba(Integer pId) {
        this.pId = pId;
    }

    public Prueba(Integer pId, int pReferencia, int pSede) {
        this.pId = pId;
        this.pReferencia = pReferencia;
        this.pSede = pSede;
    }

    public Integer getPId() {
        return pId;
    }

    public void setPId(Integer pId) {
        this.pId = pId;
    }

    public int getPReferencia() {
        return pReferencia;
    }

    public void setPReferencia(int pReferencia) {
        this.pReferencia = pReferencia;
    }

    public int getPSede() {
        return pSede;
    }

    public void setPSede(int pSede) {
        this.pSede = pSede;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pId != null ? pId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prueba)) {
            return false;
        }
        Prueba other = (Prueba) object;
        if ((this.pId == null && other.pId != null) || (this.pId != null && !this.pId.equals(other.pId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Prueba[ pId=" + pId + " ]";
    }
    
}
