/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "referencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Referencia.findAll", query = "SELECT r FROM Referencia r")
    , @NamedQuery(name = "Referencia.findByRefId", query = "SELECT r FROM Referencia r WHERE r.refId = :refId")
    , @NamedQuery(name = "Referencia.findByRefFoto", query = "SELECT r FROM Referencia r WHERE r.refFoto = :refFoto")
    , @NamedQuery(name = "Referencia.findByRefNombre", query = "SELECT r FROM Referencia r WHERE r.refNombre = :refNombre")
    , @NamedQuery(name = "Referencia.findByRefNota", query = "SELECT r FROM Referencia r WHERE r.refNota = :refNota")
    , @NamedQuery(name = "Referencia.findByRefCantidadStock", query = "SELECT r FROM Referencia r WHERE r.refCantidadStock = :refCantidadStock")})
public class Referencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ref_id")
    private Integer refId;
    @Size(max = 100)
    @Column(name = "ref_foto")
    private String refFoto;
    @Size(max = 40)
    @Column(name = "ref_nombre")
    private String refNombre;
    @Size(max = 255)
    @Column(name = "ref_nota")
    private String refNota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ref_cantidad_stock")
    private int refCantidadStock;
    @OneToMany(mappedBy = "comReferencia")
    private Collection<Compra> compraCollection;
    @OneToMany(mappedBy = "venReferencia")
    private Collection<Venta> ventaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "referencia")
    private Collection<ReferenciaSede> referenciaSedeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "referencia")
    private Collection<ReferenciaListaPrecio> referenciaListaPrecioCollection;
    @JoinColumn(name = "ref_linea", referencedColumnName = "lin_id")
    @ManyToOne
    private Linea refLinea;
    @JoinColumn(name = "ref_marca", referencedColumnName = "mar_id")
    @ManyToOne
    private Marca refMarca;

    public Referencia() {
    }

    public Referencia(Integer refId) {
        this.refId = refId;
    }

    public Referencia(Integer refId, int refCantidadStock) {
        this.refId = refId;
        this.refCantidadStock = refCantidadStock;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getRefFoto() {
        return refFoto;
    }

    public void setRefFoto(String refFoto) {
        this.refFoto = refFoto;
    }

    public String getRefNombre() {
        return refNombre;
    }

    public void setRefNombre(String refNombre) {
        this.refNombre = refNombre;
    }

    public String getRefNota() {
        return refNota;
    }

    public void setRefNota(String refNota) {
        this.refNota = refNota;
    }

    public int getRefCantidadStock() {
        return refCantidadStock;
    }

    public void setRefCantidadStock(int refCantidadStock) {
        this.refCantidadStock = refCantidadStock;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    @XmlTransient
    public Collection<ReferenciaSede> getReferenciaSedeCollection() {
        return referenciaSedeCollection;
    }

    public void setReferenciaSedeCollection(Collection<ReferenciaSede> referenciaSedeCollection) {
        this.referenciaSedeCollection = referenciaSedeCollection;
    }

    @XmlTransient
    public Collection<ReferenciaListaPrecio> getReferenciaListaPrecioCollection() {
        return referenciaListaPrecioCollection;
    }

    public void setReferenciaListaPrecioCollection(Collection<ReferenciaListaPrecio> referenciaListaPrecioCollection) {
        this.referenciaListaPrecioCollection = referenciaListaPrecioCollection;
    }

    public Linea getRefLinea() {
        return refLinea;
    }

    public void setRefLinea(Linea refLinea) {
        this.refLinea = refLinea;
    }

    public Marca getRefMarca() {
        return refMarca;
    }

    public void setRefMarca(Marca refMarca) {
        this.refMarca = refMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refId != null ? refId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Referencia)) {
            return false;
        }
        Referencia other = (Referencia) object;
        if ((this.refId == null && other.refId != null) || (this.refId != null && !this.refId.equals(other.refId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Referencia[ refId=" + refId + " ]";
    }
    
}
