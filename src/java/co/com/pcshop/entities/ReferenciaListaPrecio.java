/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "referencia_lista_precio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReferenciaListaPrecio.findAll", query = "SELECT r FROM ReferenciaListaPrecio r")
    , @NamedQuery(name = "ReferenciaListaPrecio.findByReflispreReferencia", query = "SELECT r FROM ReferenciaListaPrecio r WHERE r.referenciaListaPrecioPK.reflispreReferencia = :reflispreReferencia")
    , @NamedQuery(name = "ReferenciaListaPrecio.findByReflispreListaPrecio", query = "SELECT r FROM ReferenciaListaPrecio r WHERE r.referenciaListaPrecioPK.reflispreListaPrecio = :reflispreListaPrecio")
    , @NamedQuery(name = "ReferenciaListaPrecio.findByReflispreValor", query = "SELECT r FROM ReferenciaListaPrecio r WHERE r.reflispreValor = :reflispreValor")
    , @NamedQuery(name = "ReferenciaListaPrecio.findByReflispreDescuento", query = "SELECT r FROM ReferenciaListaPrecio r WHERE r.reflispreDescuento = :reflispreDescuento")})
public class ReferenciaListaPrecio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReferenciaListaPrecioPK referenciaListaPrecioPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_valor")
    private double reflispreValor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_descuento")
    private double reflispreDescuento;
    @JoinColumn(name = "reflispre_lista_precio", referencedColumnName = "lispre_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ListaPrecio listaPrecio;
    @JoinColumn(name = "reflispre_referencia", referencedColumnName = "ref_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Referencia referencia;

    public ReferenciaListaPrecio() {
    }

    public ReferenciaListaPrecio(ReferenciaListaPrecioPK referenciaListaPrecioPK) {
        this.referenciaListaPrecioPK = referenciaListaPrecioPK;
    }

    public ReferenciaListaPrecio(ReferenciaListaPrecioPK referenciaListaPrecioPK, double reflispreValor, double reflispreDescuento) {
        this.referenciaListaPrecioPK = referenciaListaPrecioPK;
        this.reflispreValor = reflispreValor;
        this.reflispreDescuento = reflispreDescuento;
    }

    public ReferenciaListaPrecio(int reflispreReferencia, int reflispreListaPrecio) {
        this.referenciaListaPrecioPK = new ReferenciaListaPrecioPK(reflispreReferencia, reflispreListaPrecio);
    }

    public ReferenciaListaPrecioPK getReferenciaListaPrecioPK() {
        return referenciaListaPrecioPK;
    }

    public void setReferenciaListaPrecioPK(ReferenciaListaPrecioPK referenciaListaPrecioPK) {
        this.referenciaListaPrecioPK = referenciaListaPrecioPK;
    }

    public double getReflispreValor() {
        return reflispreValor;
    }

    public void setReflispreValor(double reflispreValor) {
        this.reflispreValor = reflispreValor;
    }

    public double getReflispreDescuento() {
        return reflispreDescuento;
    }

    public void setReflispreDescuento(double reflispreDescuento) {
        this.reflispreDescuento = reflispreDescuento;
    }

    public ListaPrecio getListaPrecio() {
        return listaPrecio;
    }

    public void setListaPrecio(ListaPrecio listaPrecio) {
        this.listaPrecio = listaPrecio;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referenciaListaPrecioPK != null ? referenciaListaPrecioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReferenciaListaPrecio)) {
            return false;
        }
        ReferenciaListaPrecio other = (ReferenciaListaPrecio) object;
        if ((this.referenciaListaPrecioPK == null && other.referenciaListaPrecioPK != null) || (this.referenciaListaPrecioPK != null && !this.referenciaListaPrecioPK.equals(other.referenciaListaPrecioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.ReferenciaListaPrecio[ referenciaListaPrecioPK=" + referenciaListaPrecioPK + " ]";
    }
    
}
