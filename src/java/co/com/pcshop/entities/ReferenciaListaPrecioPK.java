/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Usuario
 */
@Embeddable
public class ReferenciaListaPrecioPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_referencia")
    private int reflispreReferencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reflispre_lista_precio")
    private int reflispreListaPrecio;

    public ReferenciaListaPrecioPK() {
    }

    public ReferenciaListaPrecioPK(int reflispreReferencia, int reflispreListaPrecio) {
        this.reflispreReferencia = reflispreReferencia;
        this.reflispreListaPrecio = reflispreListaPrecio;
    }

    public int getReflispreReferencia() {
        return reflispreReferencia;
    }

    public void setReflispreReferencia(int reflispreReferencia) {
        this.reflispreReferencia = reflispreReferencia;
    }

    public int getReflispreListaPrecio() {
        return reflispreListaPrecio;
    }

    public void setReflispreListaPrecio(int reflispreListaPrecio) {
        this.reflispreListaPrecio = reflispreListaPrecio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) reflispreReferencia;
        hash += (int) reflispreListaPrecio;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReferenciaListaPrecioPK)) {
            return false;
        }
        ReferenciaListaPrecioPK other = (ReferenciaListaPrecioPK) object;
        if (this.reflispreReferencia != other.reflispreReferencia) {
            return false;
        }
        if (this.reflispreListaPrecio != other.reflispreListaPrecio) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.ReferenciaListaPrecioPK[ reflispreReferencia=" + reflispreReferencia + ", reflispreListaPrecio=" + reflispreListaPrecio + " ]";
    }
    
}
