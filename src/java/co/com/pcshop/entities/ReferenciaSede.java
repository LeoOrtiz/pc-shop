/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "referencia_sede")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReferenciaSede.findAll", query = "SELECT r FROM ReferenciaSede r")
    , @NamedQuery(name = "ReferenciaSede.findByRefsedReferencia", query = "SELECT r FROM ReferenciaSede r WHERE r.referenciaSedePK.refsedReferencia = :refsedReferencia")
    , @NamedQuery(name = "ReferenciaSede.findByRefsedSede", query = "SELECT r FROM ReferenciaSede r WHERE r.referenciaSedePK.refsedSede = :refsedSede")
    , @NamedQuery(name = "ReferenciaSede.findByRefsedCantidad", query = "SELECT r FROM ReferenciaSede r WHERE r.refsedCantidad = :refsedCantidad")})
public class ReferenciaSede implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReferenciaSedePK referenciaSedePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_cantidad")
    private int refsedCantidad;
    @JoinColumn(name = "refsed_sede", referencedColumnName = "sed_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sede sede;
    @JoinColumn(name = "refsed_referencia", referencedColumnName = "ref_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Referencia referencia;

    public ReferenciaSede() {
    }

    public ReferenciaSede(ReferenciaSedePK referenciaSedePK) {
        this.referenciaSedePK = referenciaSedePK;
    }

    public ReferenciaSede(ReferenciaSedePK referenciaSedePK, int refsedCantidad) {
        this.referenciaSedePK = referenciaSedePK;
        this.refsedCantidad = refsedCantidad;
    }

    public ReferenciaSede(int refsedReferencia, int refsedSede) {
        this.referenciaSedePK = new ReferenciaSedePK(refsedReferencia, refsedSede);
    }

    public ReferenciaSedePK getReferenciaSedePK() {
        return referenciaSedePK;
    }

    public void setReferenciaSedePK(ReferenciaSedePK referenciaSedePK) {
        this.referenciaSedePK = referenciaSedePK;
    }

    public int getRefsedCantidad() {
        return refsedCantidad;
    }

    public void setRefsedCantidad(int refsedCantidad) {
        this.refsedCantidad = refsedCantidad;
    }

    public Sede getSede() {
        return sede;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referenciaSedePK != null ? referenciaSedePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReferenciaSede)) {
            return false;
        }
        ReferenciaSede other = (ReferenciaSede) object;
        if ((this.referenciaSedePK == null && other.referenciaSedePK != null) || (this.referenciaSedePK != null && !this.referenciaSedePK.equals(other.referenciaSedePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.ReferenciaSede[ referenciaSedePK=" + referenciaSedePK + " ]";
    }
    
}
