/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Usuario
 */
@Embeddable
public class ReferenciaSedePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_referencia")
    private int refsedReferencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refsed_sede")
    private int refsedSede;

    public ReferenciaSedePK() {
    }

    public ReferenciaSedePK(int refsedReferencia, int refsedSede) {
        this.refsedReferencia = refsedReferencia;
        this.refsedSede = refsedSede;
    }

    public int getRefsedReferencia() {
        return refsedReferencia;
    }

    public void setRefsedReferencia(int refsedReferencia) {
        this.refsedReferencia = refsedReferencia;
    }

    public int getRefsedSede() {
        return refsedSede;
    }

    public void setRefsedSede(int refsedSede) {
        this.refsedSede = refsedSede;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) refsedReferencia;
        hash += (int) refsedSede;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReferenciaSedePK)) {
            return false;
        }
        ReferenciaSedePK other = (ReferenciaSedePK) object;
        if (this.refsedReferencia != other.refsedReferencia) {
            return false;
        }
        if (this.refsedSede != other.refsedSede) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.ReferenciaSedePK[ refsedReferencia=" + refsedReferencia + ", refsedSede=" + refsedSede + " ]";
    }
    
}
