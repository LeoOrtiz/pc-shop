/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "sede")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sede.findAll", query = "SELECT s FROM Sede s")
    , @NamedQuery(name = "Sede.findBySedId", query = "SELECT s FROM Sede s WHERE s.sedId = :sedId")
    , @NamedQuery(name = "Sede.findBySedNombre", query = "SELECT s FROM Sede s WHERE s.sedNombre = :sedNombre")
    , @NamedQuery(name = "Sede.findBySedFoto", query = "SELECT s FROM Sede s WHERE s.sedFoto = :sedFoto")
    , @NamedQuery(name = "Sede.findBySedDireccion", query = "SELECT s FROM Sede s WHERE s.sedDireccion = :sedDireccion")
    , @NamedQuery(name = "Sede.findBySedTelefono", query = "SELECT s FROM Sede s WHERE s.sedTelefono = :sedTelefono")})
public class Sede implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sed_id")
    private Integer sedId;
    @Size(max = 40)
    @Column(name = "sed_nombre")
    private String sedNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sed_foto")
    private String sedFoto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "sed_direccion")
    private String sedDireccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "sed_telefono")
    private String sedTelefono;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comSede")
    private Collection<Compra> compraCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "venSede")
    private Collection<Venta> ventaCollection;
    @JoinColumn(name = "sed_empresa", referencedColumnName = "emp_id")
    @ManyToOne
    private Empresa sedEmpresa;
    @JoinColumn(name = "sed_ubicacion", referencedColumnName = "ubi_id")
    @ManyToOne
    private Ubicacion sedUbicacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sede")
    private Collection<ReferenciaSede> referenciaSedeCollection;

    public Sede() {
    }

    public Sede(Integer sedId) {
        this.sedId = sedId;
    }

    public Sede(Integer sedId, String sedFoto, String sedDireccion, String sedTelefono) {
        this.sedId = sedId;
        this.sedFoto = sedFoto;
        this.sedDireccion = sedDireccion;
        this.sedTelefono = sedTelefono;
    }

    public Integer getSedId() {
        return sedId;
    }

    public void setSedId(Integer sedId) {
        this.sedId = sedId;
    }

    public String getSedNombre() {
        return sedNombre;
    }

    public void setSedNombre(String sedNombre) {
        this.sedNombre = sedNombre;
    }

    public String getSedFoto() {
        return sedFoto;
    }

    public void setSedFoto(String sedFoto) {
        this.sedFoto = sedFoto;
    }

    public String getSedDireccion() {
        return sedDireccion;
    }

    public void setSedDireccion(String sedDireccion) {
        this.sedDireccion = sedDireccion;
    }

    public String getSedTelefono() {
        return sedTelefono;
    }

    public void setSedTelefono(String sedTelefono) {
        this.sedTelefono = sedTelefono;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    public Empresa getSedEmpresa() {
        return sedEmpresa;
    }

    public void setSedEmpresa(Empresa sedEmpresa) {
        this.sedEmpresa = sedEmpresa;
    }

    public Ubicacion getSedUbicacion() {
        return sedUbicacion;
    }

    public void setSedUbicacion(Ubicacion sedUbicacion) {
        this.sedUbicacion = sedUbicacion;
    }

    @XmlTransient
    public Collection<ReferenciaSede> getReferenciaSedeCollection() {
        return referenciaSedeCollection;
    }

    public void setReferenciaSedeCollection(Collection<ReferenciaSede> referenciaSedeCollection) {
        this.referenciaSedeCollection = referenciaSedeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sedId != null ? sedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sede)) {
            return false;
        }
        Sede other = (Sede) object;
        if ((this.sedId == null && other.sedId != null) || (this.sedId != null && !this.sedId.equals(other.sedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Sede[ sedId=" + sedId + " ]";
    }
    
}
