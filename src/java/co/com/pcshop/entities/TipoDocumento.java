/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "tipo_documento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoDocumento.findAll", query = "SELECT t FROM TipoDocumento t")
    , @NamedQuery(name = "TipoDocumento.findByTipdocId", query = "SELECT t FROM TipoDocumento t WHERE t.tipdocId = :tipdocId")
    , @NamedQuery(name = "TipoDocumento.findByTipdocNombre", query = "SELECT t FROM TipoDocumento t WHERE t.tipdocNombre = :tipdocNombre")
    , @NamedQuery(name = "TipoDocumento.findByTipdocAbreviatura", query = "SELECT t FROM TipoDocumento t WHERE t.tipdocAbreviatura = :tipdocAbreviatura")})
public class TipoDocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipdoc_id")
    private Integer tipdocId;
    @Size(max = 45)
    @Column(name = "tipdoc_nombre")
    private String tipdocNombre;
    @Size(max = 3)
    @Column(name = "tipdoc_abreviatura")
    private String tipdocAbreviatura;
    @OneToMany(mappedBy = "perTipoDocumento")
    private Collection<Persona> personaCollection;
    @OneToMany(mappedBy = "proTipoDocumento")
    private Collection<Proveedor> proveedorCollection;

    public TipoDocumento() {
    }

    public TipoDocumento(Integer tipdocId) {
        this.tipdocId = tipdocId;
    }

    public Integer getTipdocId() {
        return tipdocId;
    }

    public void setTipdocId(Integer tipdocId) {
        this.tipdocId = tipdocId;
    }

    public String getTipdocNombre() {
        return tipdocNombre;
    }

    public void setTipdocNombre(String tipdocNombre) {
        this.tipdocNombre = tipdocNombre;
    }

    public String getTipdocAbreviatura() {
        return tipdocAbreviatura;
    }

    public void setTipdocAbreviatura(String tipdocAbreviatura) {
        this.tipdocAbreviatura = tipdocAbreviatura;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }
    
    @XmlTransient
    public Collection<Proveedor> getProveedorCollection() {
        return proveedorCollection;
    }

    public void setProveedorCollection(Collection<Proveedor> proveedorCollection) {
        this.proveedorCollection = proveedorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipdocId != null ? tipdocId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDocumento)) {
            return false;
        }
        TipoDocumento other = (TipoDocumento) object;
        if ((this.tipdocId == null && other.tipdocId != null) || (this.tipdocId != null && !this.tipdocId.equals(other.tipdocId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.TipoDocumento[ tipdocId=" + tipdocId + " ]";
    }
    
}
