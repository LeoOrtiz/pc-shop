/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "tipo_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoUsuario.findAll", query = "SELECT t FROM TipoUsuario t")
    , @NamedQuery(name = "TipoUsuario.findByTipusuId", query = "SELECT t FROM TipoUsuario t WHERE t.tipusuId = :tipusuId")
    , @NamedQuery(name = "TipoUsuario.findByTipusuNombre", query = "SELECT t FROM TipoUsuario t WHERE t.tipusuNombre = :tipusuNombre")})
public class TipoUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipusu_id")
    private Integer tipusuId;
    @Size(max = 100)
    @Column(name = "tipusu_nombre")
    private String tipusuNombre;
    @OneToMany(mappedBy = "menuTipoUsuario")
    private Collection<Menu> menuCollection;
    @OneToMany(mappedBy = "usuTipoUsuario")
    private Collection<Usuario> usuarioCollection;

    public TipoUsuario() {
    }

    public TipoUsuario(Integer tipusuId) {
        this.tipusuId = tipusuId;
    }

    public Integer getTipusuId() {
        return tipusuId;
    }

    public void setTipusuId(Integer tipusuId) {
        this.tipusuId = tipusuId;
    }

    public String getTipusuNombre() {
        return tipusuNombre;
    }

    public void setTipusuNombre(String tipusuNombre) {
        this.tipusuNombre = tipusuNombre;
    }

    @XmlTransient
    public Collection<Menu> getMenuCollection() {
        return menuCollection;
    }

    public void setMenuCollection(Collection<Menu> menuCollection) {
        this.menuCollection = menuCollection;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipusuId != null ? tipusuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoUsuario)) {
            return false;
        }
        TipoUsuario other = (TipoUsuario) object;
        if ((this.tipusuId == null && other.tipusuId != null) || (this.tipusuId != null && !this.tipusuId.equals(other.tipusuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.TipoUsuario[ tipusuId=" + tipusuId + " ]";
    }
    
}
