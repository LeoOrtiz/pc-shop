/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "ubicacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ubicacion.findAll", query = "SELECT u FROM Ubicacion u")
    , @NamedQuery(name = "Ubicacion.findByUbiId", query = "SELECT u FROM Ubicacion u WHERE u.ubiId = :ubiId")
    , @NamedQuery(name = "Ubicacion.findByUbiLatitud", query = "SELECT u FROM Ubicacion u WHERE u.ubiLatitud = :ubiLatitud")
    , @NamedQuery(name = "Ubicacion.findByUbiLongitud", query = "SELECT u FROM Ubicacion u WHERE u.ubiLongitud = :ubiLongitud")})
public class Ubicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ubi_id")
    private Integer ubiId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ubi_latitud")
    private Double ubiLatitud;
    @Column(name = "ubi_longitud")
    private Double ubiLongitud;
    @OneToMany(mappedBy = "sedUbicacion")
    private Collection<Sede> sedeCollection;

    public Ubicacion() {
    }

    public Ubicacion(Integer ubiId) {
        this.ubiId = ubiId;
    }

    public Integer getUbiId() {
        return ubiId;
    }

    public void setUbiId(Integer ubiId) {
        this.ubiId = ubiId;
    }

    public Double getUbiLatitud() {
        return ubiLatitud;
    }

    public void setUbiLatitud(Double ubiLatitud) {
        this.ubiLatitud = ubiLatitud;
    }

    public Double getUbiLongitud() {
        return ubiLongitud;
    }

    public void setUbiLongitud(Double ubiLongitud) {
        this.ubiLongitud = ubiLongitud;
    }

    @XmlTransient
    public Collection<Sede> getSedeCollection() {
        return sedeCollection;
    }

    public void setSedeCollection(Collection<Sede> sedeCollection) {
        this.sedeCollection = sedeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ubiId != null ? ubiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ubicacion)) {
            return false;
        }
        Ubicacion other = (Ubicacion) object;
        if ((this.ubiId == null && other.ubiId != null) || (this.ubiId != null && !this.ubiId.equals(other.ubiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Ubicacion[ ubiId=" + ubiId + " ]";
    }
    
}
