/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUsuId", query = "SELECT u FROM Usuario u WHERE u.usuId = :usuId")
    , @NamedQuery(name = "Usuario.findByUsuClave", query = "SELECT u FROM Usuario u WHERE u.usuClave = :usuClave")
    , @NamedQuery(name = "Usuario.findByUsuFechaCreacion", query = "SELECT u FROM Usuario u WHERE u.usuFechaCreacion = :usuFechaCreacion")
    , @NamedQuery(name = "Usuario.findByUsuNombre", query = "SELECT u FROM Usuario u WHERE u.usuNombre = :usuNombre")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usu_id")
    private Integer usuId;
    @Size(max = 100)
    @Column(name = "usu_clave")
    private String usuClave;
    @Column(name = "usu_fecha_creacion", insertable = false, updatable = false)
    @Temporal(TemporalType.DATE)
    private Date usuFechaCreacion;
    @Size(max = 20)
    @Column(name = "usu_nombre")
    private String usuNombre;
    @OneToMany(mappedBy = "comUsuario")
    private Collection<Compra> compraCollection;
    @OneToMany(mappedBy = "venUsuario")
    private Collection<Venta> ventaCollection;
    @JoinColumn(name = "usu_estado", referencedColumnName = "est_id")
    @ManyToOne
    private Estado usuEstado;
    @JoinColumn(name = "usu_persona", referencedColumnName = "per_id")
    @ManyToOne
    private Persona usuPersona;
    @JoinColumn(name = "usu_tipo_usuario", referencedColumnName = "tipusu_id")
    @ManyToOne
    private TipoUsuario usuTipoUsuario;
    @JoinColumn(name = "usu_lista_precio", referencedColumnName = "lispre_id")
    @ManyToOne
    private ListaPrecio usuListaPrecio;

    public Usuario() {
    }

    public Usuario(Integer usuId) {
        this.usuId = usuId;
    }

    public Integer getUsuId() {
        return usuId;
    }

    public void setUsuId(Integer usuId) {
        this.usuId = usuId;
    }

    public String getUsuClave() {
        return usuClave;
    }

    public void setUsuClave(String usuClave) {
        this.usuClave = usuClave;
    }

    public Date getUsuFechaCreacion() {
        return usuFechaCreacion;
    }

    public void setUsuFechaCreacion(Date usuFechaCreacion) {
        this.usuFechaCreacion = usuFechaCreacion;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    public Estado getUsuEstado() {
        return usuEstado;
    }

    public void setUsuEstado(Estado usuEstado) {
        this.usuEstado = usuEstado;
    }

    public Persona getUsuPersona() {
        return usuPersona;
    }

    public void setUsuPersona(Persona usuPersona) {
        this.usuPersona = usuPersona;
    }

    public TipoUsuario getUsuTipoUsuario() {
        return usuTipoUsuario;
    }

    public void setUsuTipoUsuario(TipoUsuario usuTipoUsuario) {
        this.usuTipoUsuario = usuTipoUsuario;
    }
    
    public ListaPrecio getUsuListaPrecio() {
        return usuListaPrecio;
    }

    public void setUsuListaPrecio(ListaPrecio usuListaPrecio) {
        this.usuListaPrecio = usuListaPrecio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuId != null ? usuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuId == null && other.usuId != null) || (this.usuId != null && !this.usuId.equals(other.usuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Usuario[ usuId=" + usuId + " ]";
    }
    
}
