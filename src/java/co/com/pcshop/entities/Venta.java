/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "venta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Venta.findAll", query = "SELECT v FROM Venta v")
    , @NamedQuery(name = "Venta.findByVenId", query = "SELECT v FROM Venta v WHERE v.venId = :venId")
    , @NamedQuery(name = "Venta.findByVenFecha", query = "SELECT v FROM Venta v WHERE v.venFecha = :venFecha")
    , @NamedQuery(name = "Venta.findByVenSubtotal", query = "SELECT v FROM Venta v WHERE v.venSubtotal = :venSubtotal")
    , @NamedQuery(name = "Venta.findByVenIva", query = "SELECT v FROM Venta v WHERE v.venIva = :venIva")
    , @NamedQuery(name = "Venta.findByVenDescuento", query = "SELECT v FROM Venta v WHERE v.venDescuento = :venDescuento")
    , @NamedQuery(name = "Venta.findByVenTotal", query = "SELECT v FROM Venta v WHERE v.venTotal = :venTotal")
    , @NamedQuery(name = "Venta.findByVenCantidad", query = "SELECT v FROM Venta v WHERE v.venCantidad = :venCantidad")
    , @NamedQuery(name = "Venta.findByVenPrecio", query = "SELECT v FROM Venta v WHERE v.venPrecio = :venPrecio")})
public class Venta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ven_id")
    private Integer venId;
    @Column(name = "ven_fecha", insertable = false, updatable = false)
    @Temporal(TemporalType.DATE)
    private Date venFecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ven_subtotal")
    private Double venSubtotal;
    @Column(name = "ven_iva")
    private Double venIva;
    @Column(name = "ven_descuento")
    private Double venDescuento;
    @Column(name = "ven_total")
    private Double venTotal;
    @Column(name = "ven_cantidad")
    private Integer venCantidad;
    @Column(name = "ven_precio")
    private Double venPrecio;
    @JoinColumn(name = "ven_usuario", referencedColumnName = "usu_id")
    @ManyToOne
    private Usuario venUsuario;
    @JoinColumn(name = "ven_referencia", referencedColumnName = "ref_id")
    @ManyToOne
    private Referencia venReferencia;
    @JoinColumn(name = "ven_sede", referencedColumnName = "sed_id")
    @ManyToOne(optional = false)
    private Sede venSede;

    public Venta() {
    }

    public Venta(Integer venId) {
        this.venId = venId;
    }

    public Integer getVenId() {
        return venId;
    }

    public void setVenId(Integer venId) {
        this.venId = venId;
    }

    public Date getVenFecha() {
        return venFecha;
    }

    public void setVenFecha(Date venFecha) {
        this.venFecha = venFecha;
    }

    public Double getVenSubtotal() {
        return venSubtotal;
    }

    public void setVenSubtotal(Double venSubtotal) {
        this.venSubtotal = venSubtotal;
    }

    public Double getVenIva() {
        return venIva;
    }

    public void setVenIva(Double venIva) {
        this.venIva = venIva;
    }

    public Double getVenDescuento() {
        return venDescuento;
    }

    public void setVenDescuento(Double venDescuento) {
        this.venDescuento = venDescuento;
    }

    public Double getVenTotal() {
        return venTotal;
    }

    public void setVenTotal(Double venTotal) {
        this.venTotal = venTotal;
    }

    public Integer getVenCantidad() {
        return venCantidad;
    }

    public void setVenCantidad(Integer venCantidad) {
        this.venCantidad = venCantidad;
    }

    public Double getVenPrecio() {
        return venPrecio;
    }

    public void setVenPrecio(Double venPrecio) {
        this.venPrecio = venPrecio;
    }

    public Usuario getVenUsuario() {
        return venUsuario;
    }

    public void setVenUsuario(Usuario venUsuario) {
        this.venUsuario = venUsuario;
    }

    public Referencia getVenReferencia() {
        return venReferencia;
    }

    public void setVenReferencia(Referencia venReferencia) {
        this.venReferencia = venReferencia;
    }

    public Sede getVenSede() {
        return venSede;
    }

    public void setVenSede(Sede venSede) {
        this.venSede = venSede;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (venId != null ? venId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Venta)) {
            return false;
        }
        Venta other = (Venta) object;
        if ((this.venId == null && other.venId != null) || (this.venId != null && !this.venId.equals(other.venId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.pcshop.entities.Venta[ venId=" + venId + " ]";
    }
    
}
