/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.facade.local.DescuentorefLispreFacadeLocal;
import co.com.pcshop.entities.DescuentorefLispre;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class DescuentorefLispreFacade extends AbstractFacade<DescuentorefLispre> implements DescuentorefLispreFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DescuentorefLispreFacade() {
        super(DescuentorefLispre.class);
    }
    
}
