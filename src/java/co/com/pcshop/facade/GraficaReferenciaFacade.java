/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.facade.local.GraficaReferenciaFacadeLocal;
import co.com.pcshop.entities.GraficaReferencia;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class GraficaReferenciaFacade extends AbstractFacade<GraficaReferencia> implements GraficaReferenciaFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GraficaReferenciaFacade() {
        super(GraficaReferencia.class);
    }
    
}
