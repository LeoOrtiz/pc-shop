/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.facade.local.MenuFacadeLocal;
import co.com.pcshop.entities.Menu;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
public class MenuFacade extends AbstractFacade<Menu> implements MenuFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuFacade() {
        super(Menu.class);
    }

    @Override
    public List<Menu> Menu_xTipo() {
        Query qu = em.createNamedQuery("Menu.findByMenGeneral", Menu.class);
        List<Menu> listaMenu = qu.getResultList();
        return listaMenu;
    }

    @Override
    public Menu ultimoMenu() {
        Query qu = em.createNamedQuery("Menu.findByMenUltimoId", Menu.class);
        List<Menu> listaMenu = qu.getResultList();
        if (!listaMenu.isEmpty()) {
            return listaMenu.get(0);
        }
        return null;
    }
}
