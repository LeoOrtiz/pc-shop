/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.facade.local.ReferenciaFacadeLocal;
import co.com.pcshop.entities.Referencia;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
public class ReferenciaFacade extends AbstractFacade<Referencia> implements ReferenciaFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReferenciaFacade() {
        super(Referencia.class);
    }

    @Override
    public Referencia CantidadReferencias(int ref_id) {
        Query qu = em.createNamedQuery("Referencia.findByRefId", Referencia.class).setParameter("refId", ref_id);
        List<Referencia> listaReferencia = qu.getResultList();
        if (!listaReferencia.isEmpty()) {
            return listaReferencia.get(0);
        }
        return null;
    }
}
