/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.entities.ListaPrecio;
import co.com.pcshop.entities.Referencia;
import co.com.pcshop.facade.local.ReferenciaListaPrecioFacadeLocal;
import co.com.pcshop.entities.ReferenciaListaPrecio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
public class ReferenciaListaPrecioFacade extends AbstractFacade<ReferenciaListaPrecio> implements ReferenciaListaPrecioFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReferenciaListaPrecioFacade() {
        super(ReferenciaListaPrecio.class);
    }
    
}
