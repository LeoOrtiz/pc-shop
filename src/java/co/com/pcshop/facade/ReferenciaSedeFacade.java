/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.facade.local.ReferenciaSedeFacadeLocal;
import co.com.pcshop.entities.ReferenciaSede;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class ReferenciaSedeFacade extends AbstractFacade<ReferenciaSede> implements ReferenciaSedeFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReferenciaSedeFacade() {
        super(ReferenciaSede.class);
    }
    
}
