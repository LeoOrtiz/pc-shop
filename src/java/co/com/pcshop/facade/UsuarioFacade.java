/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade;

import co.com.pcshop.facade.local.UsuarioFacadeLocal;
import co.com.pcshop.entities.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "PC-SHOPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
 
    public Usuario iniciarSesion(String usu) {
        Query qu = em.createNamedQuery("Usuario.findByUsuNombre", Usuario.class).setParameter("usuNombre", usu);
        List<Usuario> listaUsuario = qu.getResultList();
        if (!listaUsuario.isEmpty()) {
            return listaUsuario.get(0);
        }
        return null;
    }

    @Override
    public Usuario findUusario(int usuId) {
        Query qu = em.createNamedQuery("Usuario.findByUsuId", Usuario.class).setParameter("usuId", usuId);
        List<Usuario> listaUsuario = qu.getResultList();
        if (!listaUsuario.isEmpty()) {
            return listaUsuario.get(0);
        }
        return null;
    }
}
