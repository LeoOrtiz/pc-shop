/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade.local;

import co.com.pcshop.entities.DescuentorefLispre;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface DescuentorefLispreFacadeLocal {

    void create(DescuentorefLispre descuentorefLispre);

    void edit(DescuentorefLispre descuentorefLispre);

    void remove(DescuentorefLispre descuentorefLispre);

    DescuentorefLispre find(Object id);

    List<DescuentorefLispre> findAll();

    List<DescuentorefLispre> findRange(int[] range);

    int count();
    
}
