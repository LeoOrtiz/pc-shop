/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade.local;

import co.com.pcshop.entities.Linea;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface LineaFacadeLocal {

    void create(Linea linea);

    void edit(Linea linea);

    void remove(Linea linea);

    Linea find(Object id);

    List<Linea> findAll();

    List<Linea> findRange(int[] range);

    int count();
    
}
