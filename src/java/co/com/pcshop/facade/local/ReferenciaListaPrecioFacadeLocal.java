/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade.local;

import co.com.pcshop.entities.ListaPrecio;
import co.com.pcshop.entities.Referencia;
import co.com.pcshop.entities.ReferenciaListaPrecio;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface ReferenciaListaPrecioFacadeLocal {

    void create(ReferenciaListaPrecio referenciaListaPrecio);

    void edit(ReferenciaListaPrecio referenciaListaPrecio);

    void remove(ReferenciaListaPrecio referenciaListaPrecio);

    ReferenciaListaPrecio find(Object id);

    List<ReferenciaListaPrecio> findAll();

    List<ReferenciaListaPrecio> findRange(int[] range);

    int count();
}
