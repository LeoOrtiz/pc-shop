/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.facade.local;

import co.com.pcshop.entities.ReferenciaSede;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface ReferenciaSedeFacadeLocal {

    void create(ReferenciaSede referenciaSede);

    void edit(ReferenciaSede referenciaSede);

    void remove(ReferenciaSede referenciaSede);

    ReferenciaSede find(Object id);

    List<ReferenciaSede> findAll();

    List<ReferenciaSede> findRange(int[] range);

    int count();
    
}
