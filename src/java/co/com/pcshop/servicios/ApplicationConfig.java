/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.servicios;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Mauricio Giraldo
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(co.com.pcshop.servicios.DescuentorefLispreFacadeREST.class);
        resources.add(co.com.pcshop.servicios.EmpresaFacadeREST.class);
        resources.add(co.com.pcshop.servicios.EstadoFacadeREST.class);
        resources.add(co.com.pcshop.servicios.GraficaReferenciaFacadeREST.class);
        resources.add(co.com.pcshop.servicios.LineaFacadeREST.class);
        resources.add(co.com.pcshop.servicios.ListaPrecioFacadeREST.class);
        resources.add(co.com.pcshop.servicios.MarcaFacadeREST.class);
        resources.add(co.com.pcshop.servicios.MenuFacadeREST.class);
        resources.add(co.com.pcshop.servicios.PersonaFacadeREST.class);
        resources.add(co.com.pcshop.servicios.ReferenciaFacadeREST.class);
        resources.add(co.com.pcshop.servicios.ReferenciaListaPrecioFacadeREST.class);
        resources.add(co.com.pcshop.servicios.SedeFacadeREST.class);
        resources.add(co.com.pcshop.servicios.TipoUsuarioFacadeREST.class);
        resources.add(co.com.pcshop.servicios.UbicacionFacadeREST.class);
        resources.add(co.com.pcshop.servicios.UsuarioFacadeREST.class);
    }
}
