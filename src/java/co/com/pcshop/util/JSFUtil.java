/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pcshop.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/**
 *
 * @author APRENDIZ
 */
public class JSFUtil {
    
    public static String getPath() {
        try {
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();
            return ctx.getRealPath("/");

        } catch (Exception e) {

            //addErrorMessage("getPath() " + e.getLocalizedMessage());
        }
        return null;

    }

    /*
     devuelve un hashmap con la ruta de fotos clinicas y el url para las imagenes
     */
    public static HashMap<String, String> getMapPathFotosClinica() {
        try {
            HashMap<String, String> map = new HashMap<String, String>();

            String path = getPath() + "clinicas/";
            map.put("path", path);
            map.put("url", "/clinicas/");
            return map;
        } catch (Exception e) {

            //addErrorMessage(" getMapPathFotosClinica() " + e.getLocalizedMessage());
        }
        return null;

    }


    /*
     devuelve un hashmap con la ruta de fotos clinicas y el url para las imagenes
     */
    public static String getPathFotosClinica() {
        try {

            String path = getPath() + "clinicas/";
            return path;
        } catch (Exception e) {

            //addErrorMessage("getPathFotosClinica() " + e.getLocalizedMessage());
        }
        return null;

    }

    /*
     copia un archivo generalmente cuando se usa el fileupload
     fileName: nombre del archivo a copiar
     in: Es el InputStream
     destination: ruta donde se guardara el archivo
  
     */
    public static Boolean copyFile(String fileName, InputStream in, String destination) {
        try {

            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(destination + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            return true;
        } catch (IOException e) {
            //JSFUtil.addErrorMessage("copyFile() " + e.getLocalizedMessage());
        }
        return false;
    }
}
