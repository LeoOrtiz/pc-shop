$(document).ready(function () {
    $("#grafica").click("click", function () {
        ListaPubli();
    });
});

function ListaPubli() {
    $.ajax({
        type: 'GET',
        //contentType: "application/json; charset=utf-8",
        url: "http://localhost:48020/PC-SHOP/webresources/referencia",
        data: "{}",
        dataType: "json",
        success: function (Result1) {
            //console.log(Result);
            //Result = Result.d;
            var data1 = [];
            for (var i in Result1) {
//                alert("hola");
                var serie = new Array(Result1[i].refNombre, Result1[i].refCantidadStock);
//                if (Result1[i].pronombreproducto == 3) {
//                    data1.push(serie);
//                }
                console.log(serie);
                data1.push(serie);

            }
            DibujaGrafico(data1);
            //DreawChartPubli(data, 'container2');

        },
        error: function (Result1) {
            alert("Error");
        }
    });
}

function DibujaGrafico(series1) {
    $('#graficareferencias').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1, //null,
            plotShadow: false
        },
        title: {
            text: 'Inventario de Referencias'
        },
        subtitle: {
        },
        tooltip: {
            pointFormat: '{series1.name}: {point.y} (<b>{point.percentage:.1f}%</b>)'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
                type: 'pie',
                name: 'Publicidad1',
                data: series1
            }]
    });
}