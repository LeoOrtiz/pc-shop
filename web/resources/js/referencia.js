/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mymapreferencia;

var selectedValue_refId;
var textValue_lispreId;
var NombreReferencia;

function getSelectValue() {
    selectedValue_refId = PF('wv_idreferencia').getSelectedValue();  
    textValue_lispreId = document.getElementById("lispreId").value;

    mymapreferencia = L.DomUtil.get('mapreferencia'); if(mymapreferencia != null){ mymapreferencia._leaflet_id = null; }
    mymapreferencia = L.map('mapreferencia').setView([5.06811, -75.51732], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 22,
        minZoom: 10,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11'
    }).addTo(mymapreferencia);
    sede();

    function sede() {
        try {
            $.ajax({
                url: "http://localhost:48020/PC-SHOP/webresources/descuentoreflispre",
                type: 'GET',
                dataType: 'json',
                context: document.body,
                data: {
                    // "id": 1
                },
                success: function (datos) {
                    mostrar(datos);
                }
            });
        } catch (e) {
            alert(e.message);
        }
    }
    function mostrar(datos) {
        console.log(datos);
        var sede = datos;
        for (var i = 0; i < sede.length; i++) {
            if (sede[i].refId == selectedValue_refId && sede[i].lispreId == textValue_lispreId) {
                if (sede[i].reflispreDescuento <= 0) {
                    NombreReferencia = sede[i].refNombre;
                    var latitud = sede[i].ubiLatitud;
                    var longitud = sede[i].ubiLongitud;
                    var nombre = sede[i].refNombre;
                    var foto = sede[i].refFoto;
                    var listaprecio = sede[i].lispreNombre;
                    var marca = sede[i].marFoto;
                    var precio = sede[i].reflispreValor;
                    var descuento = sede[i].reflispreDescuento;
                    var stock = sede[i].refsedCantidad;
                    var id = sede[i].refId;
                    var sedenombre = sede[i].sedNombre;
                    var icono;
                    icono = L.icon({
                        iconUrl: 'http://localhost:48020/PC-SHOP/resources/images/icon-pc.png',
                        iconSize: [33, 33]
                    });
                    addMarker1(latitud, longitud, nombre, foto, listaprecio, marca, precio, descuento, stock, sedenombre, id, icono);
                } else {
                    NombreReferencia = sede[i].refNombre;
                    var latitud = sede[i].ubiLatitud;
                    var longitud = sede[i].ubiLongitud;
                    var nombre = sede[i].refNombre;
                    var foto = sede[i].refFoto;
                    var listaprecio = sede[i].lispreNombre;
                    var marca = sede[i].marFoto;
                    var precio = sede[i].reflispreValor;
                    var descuento = sede[i].reflispreDescuento;
                    var stock = sede[i].refsedCantidad;
                    var id = sede[i].refId;
                    var sedenombre = sede[i].sedNombre;
                    var icono;
                    icono = L.icon({
                        iconUrl: 'http://localhost:48020/PC-SHOP/resources/images/oferta.png',
                        iconSize: [33, 33]
                    });
                    addMarker2(latitud, longitud, nombre, foto, listaprecio, marca, precio, descuento, stock, sedenombre, id, icono);
                }
            }
        }
    }
    function addMarker1(latitud, longitud, nombre, foto, listaprecio, marca, precio, descuento, stock, sedenombre, content, icon) {
        L.marker([latitud, longitud], {icon: icon}).addTo(mymapreferencia)
                .bindPopup("<b><h4>" + nombre + "</h4></b>" +
                        "<img style='text-align: left; height: 30px;width: 30px; margin: 0 auto; display: block'; src='http://localhost:48020/PC-SHOP/resources/images/marcas/" + marca + "'/>" +
                        "<img style='float: left; height: 120px;width: 150px; margin: 0px 15px 15px 0px; display: block'; src='http://localhost:48020/PC-SHOP/resources/images/referencias/" + foto + "'/>" +
                        "<b>Lista de Precio: </b>" + listaprecio +
                        "<br/> <b>Precio: </b>" + precio +
                        "<br/> <b>Unidades en Stock: </b>" + stock +
                        "<br/> <b>Sede: </b>" + sedenombre +
                        "<form action='/PC-SHOP/faces/Referencia/ReferenciaSede_Compras.xhtml'>"+
                        "<center><input type='submit' value='Comprar' style='text-align: center;font-size: 18px;color:#000000;background-color:#DA0535;'/></center>" +
                        "</form>");
    }
    function addMarker2(latitud, longitud, nombre, foto, listaprecio, marca, precio, descuento, stock, sedenombre, content, icon) {
        L.marker([latitud, longitud], {icon: icon}).addTo(mymapreferencia)
                .bindPopup("<b><h4>" + nombre + "</h4></b>" +
                        "<img style='text-align: left; height: 30px;width: 30px; margin: 0 auto; display: block'; src='http://localhost:48020/PC-SHOP/resources/images/marcas/" + marca + "'/>" +
                        "<img style='float: left; height: 120px;width: 150px; margin: 0px 15px 15px 0px; display: block'; src='http://localhost:48020/PC-SHOP/resources/images/referencias/" + foto + "'/>" +
                        "<b>Lista de Precio: </b>" + listaprecio +
                        "<br/> <b>Precio: </b>" +
                        "<br/> <del>Antes: " + precio + "</del>" +
                        "<br/> <b>Ahora: </b>" + (precio - descuento) +
                        "<br/> <b>Unidades en Stock: </b>" + stock +
                        "<br/> <b>Sede: </b>" + sedenombre +
                        "<form action='/PC-SHOP/faces/Referencia/ReferenciaSede_Compras.xhtml'>"+
                        "<center><input type='submit' value='Comprar' style='text-align: center;font-size: 18px;color:#000000;background-color:#DA0535;'/></center>" +
                        "</form>");
    }
    
}




$(document).ready(function () {
    $("#grafica").click("click", function () {
        ListaPubli();
    });
});

function ListaPubli() {
    $.ajax({
        type: 'GET',
        //contentType: "application/json; charset=utf-8",
        url: "http://localhost:48020/PC-SHOP/webresources/graficareferencia",
        data: "{}",
        dataType: "json",
        success: function (Result1) {
            //console.log(Result);
            //Result = Result.d;
            var data1 = [];
            for (var i in Result1) {
//                alert("hola");
                var serie = new Array(Result1[i].sedNombre, Result1[i].refsedCantidad);
                if (Result1[i].refId == selectedValue_refId) {
                    data1.push(serie);
                }

            }
            DibujaGrafico(data1);
            //DreawChartPubli(data, 'container2');

        },
        error: function (Result1) {
            alert("Error");
        }
    });
}

function DibujaGrafico(series1) {
    $('#graficareferencias').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1, //null,
            plotShadow: false
        },
        title: {
            text: 'Inventario de ' + NombreReferencia
        },
        subtitle: {
        },
        tooltip: {
            pointFormat: '{series1.name}: {point.y} (<b>{point.percentage:.1f}%</b>)'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
                type: 'pie',
                name: 'Publicidad1',
                data: series1
            }]
    });
}
